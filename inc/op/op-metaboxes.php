<?php
if (! function_exists('den_register_meta_boxes')) :
  function den_register_meta_boxes($meta_boxes) {
    global $prefix;
    $location = get_stylesheet_directory_uri();
        
    $meta_boxes[] = array(
      'title'      => esc_html__( 'No es destacado', 'dreamstheme' ),
      'id'         => $prefix . 'noDest',
      'post_types' => array( 'post'),
      'context'    => 'side',
      'autosave'   => true,
      'fields'     => array(
        array(
          'name' => '<img style="max-width:100%;" src="'. $location . '/img/featured.svg">',
          'id'   => $prefix . 'sidebarCheck',
          'type' => 'checkbox'
        ),
      )
    );      
    
    
    /* $meta_boxes[] = array(
			'title'      => esc_html__('Posicion Articulo', 'dreamstheme'),
			'id'         => $prefix . 'select-layout-post',
			'priority'   => 'high',
			'autosave'   => true,
			'post_types' => array('post'),
			'fields'     => array (
				array (
					'id'               => $prefix . 'layouts-post',
					'name'             => esc_html__( 'Layout de los articulos', 'dreamstheme' ),
					'force_delete'     => false,
					'max_file_uploads' => '4',
					'type'             => 'image_select',
					'max_file_uploads' => '4',
					'options'          => array (
						'layout-child-0' => $location . '/img/layout0.png',
						'layout-child-1' => $location . '/img/layout1.png',
						'layout-child-2' => $location . '/img/layout2.png',
						'layout-child-3' => $location . '/img/layout3.png',
						'layout-child-4' => $location . '/img/layout4.png'
					),
					'std'              => 'front-0',
				)
			),
	); */
	
	/* Metabox - Video*/

	    // $meta_boxes[] = array(
		//   'title'    => esc_html__( 'Video', 'dreamstheme' ),
		//   'id'       => $prefix . 'video',
		//   'autosave' => true,
		//   'pages'    => array( 'post', 'media_videos', 'forbes_life' ),
		//   'show'     => array(
		// 	'relation'    => 'OR',
		// 	'post_format' => array( 'Video' ),
		//   ),
		  
		//   'fields'   => array(
		//    	array(
		//   		'name' => esc_html__( 'Agregar Video oembed (Solo URL)', 'dreamstheme' ),
		//    		'id'   => $prefix . 'videos_oembed',
		//   		'type' => 'oembed',
		//   	),
		//   	array(
		//   		'name' => esc_html__( 'Agregar Video embed code (iframe)', 'dreamstheme' ),
		//   		'id'   => $prefix . 'videos_embed',
		//   		'type' => 'textarea',
		//   	),
		//   ),
	    // );
    
    /************************************* 
		  Metabox - layouts 
		**************************************/

		
		// $meta_boxes[] = array(
		// 	'title'      => esc_html__( 'Seleccióna Layout', 'dreamstheme' ),
		// 	'id'         => $prefix . 'select-layout',
		// 	'priority'   => 'high',
		// 	'autosave'   => true,
		// 	'post_types' => array( 'post', 'page',),
		// 	'fields'     => array (
        //         array (
		// 			'id'               => $prefix . 'layouts',
		// 			'name'             => esc_html__( 'Layout', 'dreamstheme' ),
		// 			'force_delete'     => false,
		// 			'max_file_uploads' => '4',
		// 			'type'             => 'image_select',
		// 			'max_file_uploads' => '4',
		// 			'options'          => array(
		// 			  'front-0' => $location . '/img/front-0.jpg',
		// 			  'front-2' => $location . '/img/front-2.jpg',
		// 			  'front-4' => $location . '/img/front-4.jpg',
		// 			  'front-5' => $location . '/img/front-5.jpg'
		// 			),
		// 			'std'              => 'front-0',
		// 		),

		// 		array(
		// 			'id'      => $prefix . 'layout_new_url',
		// 			'name'    => esc_html__( 'URL de redirección', 'dreamstheme' ),
		// 			'type'    => 'url',
		// 			'title'   => esc_html__( 'Seleccióna la URL de redirección', 'dreamstheme' ),
		// 			'visible' => [ $prefix . 'layouts', '=', 'front-2' ]
		// 		),

		// 		array(
		// 			'id'      => $prefix . 'layout_video_url',
		// 			'name'    => esc_html__( 'URL del video', 'dreamstheme' ),
		// 			'type'    => 'oembed',
		// 			'title'   => esc_html__( 'Seleccióna la URL del video', 'dreamstheme' ),
		// 			'visible' => [ $prefix . 'layouts', '=', 'front-5' ]
		// 		),
      
        //         array(
		// 			'id'               => $prefix . 'layout_video_logo',
		// 			'name'             => esc_html__( 'Imagen de los logos (384px  x  216px)', 'dreamstheme' ),
		// 			'type'             => 'image_advanced',
		// 			'title'            => esc_html__( 'Seleccióna  la imagen de los logos', 'dreamstheme' ),
		// 			'max_file_uploads' => 1,
		// 			'visible'          => [ $prefix . 'layouts', '=', 'front-5' ]
		// 		),

		// 		array(
		// 			'id'               => $prefix . 'layout_video_background',
		// 			'name'             => esc_html__( 'Imagen de fondo', 'dreamstheme' ),
		// 			'type'             => 'image_advanced',
		// 			'title'            => esc_html__( 'Seleccióna la imagen de fondo', 'dreamstheme' ),
		// 			'max_file_uploads' => 1,
		// 			'visible'          => [ $prefix . 'layouts', '=', 'front-5' ]
		// 		),
      
        //         array(
		// 			'id'      => $prefix . 'layout_fixed_background',
		// 			'name'    => esc_html__( 'Posición de la imagen de fondo', 'dreamstheme' ),
		// 			'type'    => 'radio',
		// 			'title'   => esc_html__( 'Seleccióna la imagen de fondo', 'dreamstheme' ),
		// 			'options' => array(
		// 				'nofixed' => 'No Fijo',
		// 				'fixed'   => 'Fijo'
		// 			),
		// 			'visible' => [ $prefix . 'layouts', '=', 'front-5' ]
		// 		),
      
        //         array(
		// 			'id'      => $prefix . 'layout_links_go',
		// 			'name'    => esc_html__( 'Link general de pauta ', 'dreamstheme' ),
		// 			'type'    => 'url',
		// 			'title'   => esc_html__( 'Escribe el link al que va dirgida esta pauta', 'dreamstheme' ),
		// 			'visible' => [ $prefix . 'layouts', '=', 'front-5' ]
		// 		),

		// 	),

		// );
    
    
    return $meta_boxes;
  }

endif;



//Metaboxes
add_filter('rwmb_meta_boxes', 'den_register_meta_boxes');

