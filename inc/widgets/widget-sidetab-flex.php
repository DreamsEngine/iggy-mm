<?php

/**
 * Plugin Name: Side Flex
 */

// Creating the widget 
class cat_side extends WP_Widget
{

  function __construct()
  {
    parent::__construct(

      // Base ID of your widget
      'cat_side',

      // Widget name will appear in UI
      __('Category sidebar', 'cat_side_domain'),

      // Widget description
      array('description' => __('Displays a flex sidebar with a tabber style', 'cat_side_domain'),)
    );
  }

  // Creating widget front-end

  public function widget($args, $instance)
  {
    $category = apply_filters('widget_title', $instance['category']);
    $line_1 = apply_filters('widget_title', $instance['line_1']);
    $tagcat = apply_filters('widget_title', $instance['tagcat']);
   

    // before and after widget arguments are defined by themes
    echo $args['before_widget'];
    if (!empty($category))
      //echo $args['before_title'] . $category . $args['after_title'];

      // This is where you run the code and display the output

      // EMPIEZA LOOP
      ?>
        <div class="pinno-widget-tab-wrap left relative">
          <div class="pinno-feat1-list-wrap left relative">
            <div class="pinno-feat1-list-head-wrap left relative">
              <ul class="pinno-feat1-list-buts left relative">
                <li class="pinno-feat-col-tab active"><a href="#pinno-tab-col1"><span class="pinno-feat1-list-but"><?php echo $line_1; ?></span></a></li>
              </ul>
            </div><!--pinno-feat1-list-head-wrap-->
          <div id="pinno-tab-col1" class="pinno-feat1-list left relative pinno-tab-col-cont" style="display: block;">

      <?php
      if ($tagcat == 'cat_sel'){
        $query1 = new WP_Query(array('posts_per_page' => 5, 'category_name' => $instance['category']));
      }
      else if ($tagcat == 'tag_sel'){
        $query1 = new WP_Query(array('posts_per_page' => 5, 'tag' => $instance['category']));
      }
        while ($query1->have_posts()) {
          $query1->the_post();

      ?>
            <a href="<?php the_permalink(); ?>" rel="bookmark">
              <div class="pinno-feat1-list-cont left relative">
                                    <div class="pinno-feat1-list-out relative">
                    <div class="pinno-feat1-list-img left relative">
                    <?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
            
            <?php the_post_thumbnail('pinno-port-thumb', array( 'class' => 'attachment-pinno-small-thumb size-pinno-small-thumb wp-post-image' )); ?>
          <?php } ?>
                      <!--<img width="80" height="80" src="https://marco_mares.local/app/uploads/2021/01/b5bad0c0-8a92-3b17-9fb7-efbd663e185a-80x80.jpg" class="attachment-pinno-small-thumb size-pinno-small-thumb wp-post-image" alt="" loading="lazy" srcset="https://marco_mares.local/app/uploads/2021/01/b5bad0c0-8a92-3b17-9fb7-efbd663e185a-80x80.jpg 80w, https://marco_mares.local/app/uploads/2021/01/b5bad0c0-8a92-3b17-9fb7-efbd663e185a-150x150.jpg 150w" sizes="(max-width: 80px) 100vw, 80px"> -->
                    </div><!--pinno-feat1-list-img-->
                    <div class="pinno-feat1-list-in">
                      <div class="pinno-feat1-list-text">
                        <div class="pinno-cat-date-wrap left relative">
                          <span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative">hace 16 horas</span>
                        </div><!--pinno-cat-date-wrap-->
                        <h2><?php the_title(); ?></h2>
                      </div><!--pinno-feat1-list-text-->
                    </div><!--pinno-feat1-list-in-->
                  </div><!--pinno-feat1-list-out-->
                                </div><!--pinno-feat1-list-cont-->
            </a>
                                  

    <?php
    }
    wp_reset_postdata();
    ?>
 
    </div><!--pinno-tab-col1-->
                          </div><!--pinno-feat1-list-wrap-->
    </div>
    <?php
    
    // ACABA LOOP
    echo $args['after_widget'];
  }

  // Widget Backend 
  public function form($instance)
  {
    if (isset($instance['category'])) {
      $category = $instance['category'];
    } else {
      $category = __('New category', 'cat_side_domain');
    }
    if (isset($instance['line_1'])) {
      $line_1 = $instance['line_1'];
    } else {
      $line_1 = __('New line_1', 'pinno_home_feat_category_line_1_domain');
    }
    if (isset($instance['tagcat'])) {
      $tagcat = $instance['tagcat'];
    } else {
      $tagcat = __('New tagcat', 'pinno_home_feat_category_tagcat_domain');
    }
    
    // Widget admin form
?>
<!-- Title -->
    <p>
      <label for="<?php echo $this->get_field_id('line_1'); ?>"><?php _e('Line 1:'); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('line_1'); ?>" name="<?php echo $this->get_field_name('line_1'); ?>" type="text" value="<?php echo esc_attr($line_1); ?>" />
    </p>
    <!-- Cat or Tag -->
		<p>
			<label for="<?php echo $this->get_field_id('tagcat'); ?>">Display Posts By Category Or Tag:</label>
			<select id="<?php echo $this->get_field_id('tagcat'); ?>" name="<?php echo $this->get_field_name('tagcat'); ?>" style="width:100%;">
				<option value='cat_sel' <?php if ('cat_sel' == $instance['tagcat']) echo 'selected="selected"'; ?>>Category</option>
				<option value='tag_sel' <?php if ('tag_sel' == $instance['tagcat']) echo 'selected="selected"'; ?>>Tag</option>
			</select>
		</p>
<!-- Cat/tag slug -->
    <p>
      <label for="<?php echo $this->get_field_id('category'); ?>"><?php _e('Category:'); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('category'); ?>" name="<?php echo $this->get_field_name('category'); ?>" type="text" value="<?php echo esc_attr($category); ?>" />
    </p>
<?php
  }

  // Updating widget replacing old instances with new
  public function update($new_instance, $old_instance)
  {
    $instance = array();
    $instance['category'] = (!empty($new_instance['category'])) ? strip_tags($new_instance['category']) : '';
    $instance['line_1'] = (!empty($new_instance['line_1'])) ? strip_tags($new_instance['line_1']) : '';
    $instance['tagcat'] = (!empty($new_instance['tagcat'])) ? strip_tags($new_instance['tagcat']) : '';
    return $instance;
  }

  // Class cat_side ends here
}


// Register and load the widget
function cat_side_load_widget()
{
  register_widget('cat_side');
}
add_action('widgets_init', 'cat_side_load_widget');

?>