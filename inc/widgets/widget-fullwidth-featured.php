<?php 
/**
 * Plugin Name: Fullwidth featured
*/

// Creating the widget 
class fullwidth_featured extends WP_Widget {
  
    function __construct() {
    parent::__construct(
      
    // Base ID of your widget
    'fullwidth_featured', 
      
    // Widget name will appear in UI
    __('Fullwidth featured', 'fullwidth_featured_domain'), 
      
    // Widget description
    array( 'description' => __( 'Displays  3 posts in a row with a title floating left', 'fullwidth_featured_domain' ), ) 
    );
    }
      
    // Creating widget front-end
      
    public function widget( $args, $instance ) {
    $fullwidthCat = apply_filters( 'widget_title', $instance['title'] );
    
      
    // before and after widget arguments are defined by themes
    echo $args['before_widget'];
    if ( ! empty( $fullwidthCat ) )
    //echo $args['before_title'] . $fullwidthCat . $args['after_title'];
      
    // This is where you run the code and display the output
    
    // EMPIEZA LOOP
    ?>
    <section id="pinno-feat6-wrap" class="left relative">
    <?php global $do_not_duplicate; global $post; $recent = new WP_Query(array( 'tag' => $fullwidthCat, 'posts_per_page' => '1', 'ignore_sticky_posts'=> 1 )); while($recent->have_posts()) : $recent->the_post(); $do_not_duplicate[] = $post->ID; ?>
      <a href="<?php the_permalink(); ?>" rel="bookmark">
      <div id="pinno-feat6-main" class="left relative">
        <div id="pinno-feat6-img" class="right relative">
          <?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
            <?php the_post_thumbnail('pinno-post-thumb', array( 'class' => 'pinno-reg-img' )); ?>
            <?php the_post_thumbnail('pinno-port-thumb', array( 'class' => 'pinno-mob-img' )); ?>
          <?php } ?>
        </div><!--pinno-feat6-img-->
        <div id="pinno-feat6-text">
          <h3 class="pinno-feat1-pop-head"><span class="pinno-feat1-pop-head"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span></h3>
          <h2><span><?php the_title(); ?></span></h2>
          <p><?php echo wp_trim_words( get_the_excerpt(), 20, '...' ); ?></p>
        </div><!--pinno-feat6-text-->
      </div><!--pinno-feat6-main-->
      </a>
    <?php endwhile; wp_reset_postdata(); ?>
  </section><!--pinno-feat6-wrap-->
  <?php
    echo '<div class="wrap1200">';
    echo '<div class="pinno-widget-feat1-cont left relative">';
    
    $query1 = new WP_Query( array( 'tag' => $fullwidthCat , 'posts_per_page' => 4, 'offset' => '1' ) );
    while ( $query1->have_posts() ) { $query1->the_post();
      
      echo '<a href="';
      echo the_permalink();
      echo '" rel="bookmark">
                  <div class="pinno-widget-feat1-bot-story left relative">
                    <div class="pinno-widget-feat1-bot-img left relative">
                    <img width="400" height="240" src="';
                    echo get_the_post_thumbnail_url();
                    echo '"class="pinno-reg-img lazy wp-post-image" alt="" loading="lazy" ><img width="80" height="80" src="';
                    echo get_the_post_thumbnail_url();
                    echo '"class="pinno-mob-img lazy wp-post-image" alt="" loading="lazy" srcset="';
                    echo get_the_post_thumbnail_url();
                    echo '" sizes="(max-width: 80px) 100vw, 80px"></div><!--pinno-widget-feat1-bot-img-->
                    <div class="pinno-widget-feat1-bot-text left relative">
                      <div class="pinno-cat-date-wrap left relative">
                        <span class="pinno-cd-cat left relative">';
                    $cat=get_the_category();
                    echo $cat[0]->cat_name;
                    echo '</span><span class="pinno-cd-date left relative">2 days ago</span>
                      </div><!--pinno-cat-date-wrap-->
                      <h2>';
                      echo the_title( );
                      echo '</h2>
                    </div><!--pinno-widget-feat1-bot-text-->
                  </div><!--pinno-widget-feat1-bot-story-->
                  </a>';
    }
    wp_reset_postdata();
    echo '</div><!--pinno-widget-feat1-cont-->';
    echo '</div><!--pinno-widget-feat1-wrap-->';
    
    // ACABA LOOP
    echo $args['after_widget'];
    }
              
    // Widget Backend 
    public function form( $instance ) {
    if ( isset( $instance[ 'title' ] ) ) {
    $fullwidthCat = $instance[ 'title' ];
    }
    else {
    $fullwidthCat = __( 'New title', 'fullwidth_featured_domain' );
    }
    // Widget admin form
    ?>
    <p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Tag:' ); ?></label> 
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $fullwidthCat ); ?>" />
    </p>
    <?php 
    }
          
    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    return $instance;
    }
     
    // Class fullwidth_featured ends here
    } 
     
     
    // Register and load the widget
    function pinno_load_fullwidth() {
        register_widget( 'fullwidth_featured' );
    }
    add_action( 'widgets_init', 'pinno_load_fullwidth' );

?>
