<?php

/**
 * Plugin Name: Side Authors
 */

// Creating the widget 
class author_side extends WP_Widget
{

    function __construct()
    {
        parent::__construct(

            // Base ID of your widget
            'author_side',

            // Widget name will appear in UI
            __('author sidebar', 'author_side_domain'),

            // Widget description
            array('description' => __('Displays an auhor sidebar with a tabber style', 'author_side_domain'),)
        );
    }

    // Creating widget front-end

    public function widget($args, $instance)
    {
        $author1 = apply_filters('widget_title', $instance['author_1']);
        $author2 = apply_filters('widget_title', $instance['author_2']);
        $author3 = apply_filters('widget_title', $instance['author_3']);
        $author4 = apply_filters('widget_title', $instance['author_4']);
        $author5 = apply_filters('widget_title', $instance['author_5']);
        $line_1 = apply_filters('widget_title', $instance['line_1']);



        // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if (!empty($category))
            //echo $args['before_title'] . $category . $args['after_title'];

            // This is where you run the code and display the output

            // EMPIEZA LOOP
?>
        <div class="pinno-widget-tab-wrap left relative">
            <div class="pinno-feat1-list-wrap left relative">
                <div class="pinno-feat1-list-head-wrap left relative">
                    <ul class="pinno-feat1-list-buts left relative">
                        <li class="pinno-feat-col-tab active"><a href="#pinno-tab-col1"><span class="pinno-feat1-list-but"><?php echo $line_1; ?></span></a></li>
                    </ul>
                </div>
                <!--pinno-feat1-list-head-wrap-->
                <div id="pinno-tab-col1" class="pinno-feat1-list left relative pinno-tab-col-cont" style="display: block;">

                    <?php

                    $args_aut1 = new WP_Query(array('posts_per_page' => 1, 'author_name' => $instance['author_1']));
                    $args_aut2 = new WP_Query(array('posts_per_page' => 1, 'author_name' => $instance['author_2']));
                    $args_aut3 = new WP_Query(array('posts_per_page' => 1, 'author_name' => $instance['author_3']));
                    $args_aut4 = new WP_Query(array('posts_per_page' => 1, 'author_name' => $instance['author_4']));
                    $args_aut5 = new WP_Query(array('posts_per_page' => 1, 'author_name' => $instance['author_5']));

                   
                    while ($args_aut1->have_posts()) {
                        $args_aut1->the_post();
                        
                    ?>
                        <div>
                            <div class="pinno-feat1-list-cont left relative">
                                <div class="pinno-feat1-list-out relative">
                                    <div class="pinno-feat1-list-img left relative">
                                        <?php if ((function_exists('has_post_thumbnail')) && (has_post_thumbnail())) { ?>

                                            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                                        <?php } ?>
                                    </div>
                                    <!--pinno-feat1-list-img-->
                                    <div class="pinno-feat1-list-in">
                                        <div class="pinno-feat1-list-text">
                                            <div class="pinno-cat-date-wrap left relative">
                                                <span class="pinno-cd-cat left relative"><a class="side_author" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' )));?>"><?php echo get_the_author_link(); ?></a></span>
                                                <span class="pinno-cd-date left relative"><?php the_date(); ?></span>
                                            </div>
                                            <!--pinno-cat-date-wrap-->
                                            <a href="<?php the_permalink(); ?>" rel="bookmark"> <h2><?php the_title(); ?></h2></a>
                                        </div>
                                        <!--pinno-feat1-list-text-->
                                    </div>
                                    <!--pinno-feat1-list-in-->
                                </div>
                                <!--pinno-feat1-list-out-->
                            </div>
                            <!--pinno-feat1-list-cont-->
                        </div>
                    <?php
                    }
                    wp_reset_postdata();

                    while ($args_aut2->have_posts()) {
                        $args_aut2->the_post();
                        
                    ?>
                        <div>
                            <div class="pinno-feat1-list-cont left relative">
                                <div class="pinno-feat1-list-out relative">
                                    <div class="pinno-feat1-list-img left relative">
                                        <?php if ((function_exists('has_post_thumbnail')) && (has_post_thumbnail())) { ?>

                                            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                                        <?php } ?>
                                    </div>
                                    <!--pinno-feat1-list-img-->
                                    <div class="pinno-feat1-list-in">
                                        <div class="pinno-feat1-list-text">
                                            <div class="pinno-cat-date-wrap left relative">
                                                <span class="pinno-cd-cat left relative"><a class="side_author" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' )));?>"><?php echo get_the_author_link(); ?></a></span>
                                                <span class="pinno-cd-date left relative"><?php the_date(); ?></span>
                                            </div>
                                            <!--pinno-cat-date-wrap-->
                                            <a href="<?php the_permalink(); ?>" rel="bookmark"> <h2><?php the_title(); ?></h2></a>
                                        </div>
                                        <!--pinno-feat1-list-text-->
                                    </div>
                                    <!--pinno-feat1-list-in-->
                                </div>
                                <!--pinno-feat1-list-out-->
                            </div>
                            <!--pinno-feat1-list-cont-->
                        </div>
                    <?php
                    }
                    wp_reset_postdata();
                   
                    while ($args_aut3->have_posts()) {
                        $args_aut3->the_post();
                        
                    ?>
                        <div>
                            <div class="pinno-feat1-list-cont left relative">
                                <div class="pinno-feat1-list-out relative">
                                    <div class="pinno-feat1-list-img left relative">
                                        <?php if ((function_exists('has_post_thumbnail')) && (has_post_thumbnail())) { ?>

                                            <a href="<?php the_permalink(); ?>"> <?php the_post_thumbnail(); ?></a>
                                        <?php } ?>
                                    </div>
                                    <!--pinno-feat1-list-img-->
                                    <div class="pinno-feat1-list-in">
                                        <div class="pinno-feat1-list-text">
                                            <div class="pinno-cat-date-wrap left relative">
                                                <span class="pinno-cd-cat left relative"><a class="side_author" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' )));?>"><?php echo get_the_author_link(); ?></a></span>
                                                <span class="pinno-cd-date left relative"><?php the_date(); ?></span>
                                            </div>
                                            <!--pinno-cat-date-wrap-->
                                            <a href="<?php the_permalink(); ?>" rel="bookmark"> <h2><?php the_title(); ?></h2></a>
                                        </div>
                                        <!--pinno-feat1-list-text-->
                                    </div>
                                    <!--pinno-feat1-list-in-->
                                </div>
                                <!--pinno-feat1-list-out-->
                            </div>
                            <!--pinno-feat1-list-cont-->
                        </div>
                    <?php
                    }
                    wp_reset_postdata();
                    
                    while ($args_aut4->have_posts()) {
                        $args_aut4->the_post();
                        
                    ?>
                        <div>
                            <div class="pinno-feat1-list-cont left relative">
                                <div class="pinno-feat1-list-out relative">
                                    <div class="pinno-feat1-list-img left relative">
                                        <?php if ((function_exists('has_post_thumbnail')) && (has_post_thumbnail())) { ?>

                                            <a href="<?php the_permalink(); ?>">  <?php the_post_thumbnail(); ?></a>
                                        <?php } ?>
                                    </div>
                                    <!--pinno-feat1-list-img-->
                                    <div class="pinno-feat1-list-in">
                                        <div class="pinno-feat1-list-text">
                                            <div class="pinno-cat-date-wrap left relative">
                                                <span class="pinno-cd-cat left relative"><a class="side_author" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' )));?>"><?php echo get_the_author_link(); ?></a></span>
                                                <span class="pinno-cd-date left relative"><?php the_date(); ?></span>
                                            </div>
                                            <!--pinno-cat-date-wrap-->
                                            <a href="<?php the_permalink(); ?>" rel="bookmark"> <h2><?php the_title(); ?></h2></a>
                                        </div>
                                        <!--pinno-feat1-list-text-->
                                    </div>
                                    <!--pinno-feat1-list-in-->
                                </div>
                                <!--pinno-feat1-list-out-->
                            </div>
                            <!--pinno-feat1-list-cont-->
                        </div>
                    <?php
                    }
                    wp_reset_postdata();
                    
                    while ($args_aut5->have_posts()) {
                        $args_aut5->the_post();
                        
                    ?>
                        <div>
                            <div class="pinno-feat1-list-cont left relative">
                                <div class="pinno-feat1-list-out relative">
                                    <div class="pinno-feat1-list-img left relative">
                                        <?php if ((function_exists('has_post_thumbnail')) && (has_post_thumbnail())) { ?>

                                            <a href="<?php the_permalink(); ?>">   <?php the_post_thumbnail(); ?></a>
                                        <?php } ?>
                                    </div>
                                    <!--pinno-feat1-list-img-->
                                    <div class="pinno-feat1-list-in">
                                        <div class="pinno-feat1-list-text">
                                            <div class="pinno-cat-date-wrap left relative">
                                                <span class="pinno-cd-cat left relative"><a class="side_author" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' )));?>"><?php echo get_the_author_link(); ?></a></span>
                                                <span class="pinno-cd-date left relative"><?php the_date(); ?></span>
                                            </div>
                                            <!--pinno-cat-date-wrap-->
                                            <a href="<?php the_permalink(); ?>" rel="bookmark"> <h2><?php the_title(); ?></h2></a>
                                        </div>
                                        <!--pinno-feat1-list-text-->
                                    </div>
                                    <!--pinno-feat1-list-in-->
                                </div>
                                <!--pinno-feat1-list-out-->
                            </div>
                            <!--pinno-feat1-list-cont-->
                        </div>
                    <?php
                    }
                    wp_reset_postdata();
                    ?>

                </div>
                <!--pinno-tab-col1-->
            </div>
            <!--pinno-feat1-list-wrap-->
        </div>
    <?php

        // ACABA LOOP
        echo $args['after_widget'];
    }

    // Widget Backend 
    public function form($instance)
    {

        if (isset($instance['line_1'])) {
            $line_1 = $instance['line_1'];
        } else {
            $line_1 = __('New line_1', 'pinno_home_feat_category_line_1_domain');
        }

        if (isset($instance['author_1'])) {
            $author_1 = $instance['author_1'];
        } else {
            $author_1 = __('New author', 'pinno_home_feat_category_author_1_domain');
        }
        if (isset($instance['author_2'])) {
            $author_2 = $instance['author_2'];
        } else {
            $author_2 = __('New author', 'pinno_home_feat_category_author_2_domain');
        }
        if (isset($instance['author_3'])) {
            $author_3 = $instance['author_3'];
        } else {
            $author_3 = __('New author', 'pinno_home_feat_category_author_3_domain');
        }
        if (isset($instance['author_4'])) {
            $author_4 = $instance['author_4'];
        } else {
            $author_4 = __('New author', 'pinno_home_feat_category_author_4_domain');
        }
        if (isset($instance['author_5'])) {
            $author_5 = $instance['author_5'];
        } else {
            $author_5 = __('New author', 'pinno_home_feat_category_author_5_domain');
        }

        // Widget admin form
    ?>
        <!-- Title -->
        <p>
            <label for="<?php echo $this->get_field_id('line_1'); ?>"><?php _e('Line 1:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('line_1'); ?>" name="<?php echo $this->get_field_name('line_1'); ?>" type="text" value="<?php echo esc_attr($line_1); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('author_1'); ?>"><?php _e('Autor 1:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('author_1'); ?>" name="<?php echo $this->get_field_name('author_1'); ?>" type="text" value="<?php echo esc_attr($author_1); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('author_2'); ?>"><?php _e('Autor 2:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('author_2'); ?>" name="<?php echo $this->get_field_name('author_2'); ?>" type="text" value="<?php echo esc_attr($author_2); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('author_3'); ?>"><?php _e('Autor 3:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('author_3'); ?>" name="<?php echo $this->get_field_name('author_3'); ?>" type="text" value="<?php echo esc_attr($author_3); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('author_4'); ?>"><?php _e('Autor 4:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('author_4'); ?>" name="<?php echo $this->get_field_name('author_4'); ?>" type="text" value="<?php echo esc_attr($author_4); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('author_5'); ?>"><?php _e('Autor 5:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('author_5'); ?>" name="<?php echo $this->get_field_name('author_5'); ?>" type="text" value="<?php echo esc_attr($author_5); ?>" />
        </p>

<?php
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['line_1'] = (!empty($new_instance['line_1'])) ? strip_tags($new_instance['line_1']) : '';
        $instance['author_1'] = (!empty($new_instance['author_1'])) ? strip_tags($new_instance['author_1']) : '';
        $instance['author_2'] = (!empty($new_instance['author_2'])) ? strip_tags($new_instance['author_2']) : '';
        $instance['author_3'] = (!empty($new_instance['author_3'])) ? strip_tags($new_instance['author_3']) : '';
        $instance['author_4'] = (!empty($new_instance['author_4'])) ? strip_tags($new_instance['author_4']) : '';
        $instance['author_5'] = (!empty($new_instance['author_5'])) ? strip_tags($new_instance['author_5']) : '';
        return $instance;
    }

    // Class author_side ends here
}


// Register and load the widget
function author_side_load_widget()
{
    register_widget('author_side');
}
add_action('widgets_init', 'author_side_load_widget');

?>