<?php

/**
 * Plugin Name: CIBanco
 */

// Creating the widget 
class pinno_cibanco extends WP_Widget
{

  function __construct()
  {
    parent::__construct(

      // Base ID of your widget
      'pinno_cibanco',

      // Widget name will appear in UI
      __('CIBanco', 'pinno_cibanco_domain'),

      // Widget description
      array('description' => __('Displays  CIBanco shortcode', 'pinno_cibanco_domain'),)
    );
  }

  // Creating widget front-end

  public function widget($args, $instance)
  {
    

    // before and after widget arguments are defined by themes
    echo $args['before_widget'];
    
      //echo $args['before_title'] . $title . $args['after_title'];

      // This is where you run the code and display the output

      // EMPIEZA LOOP
     
      ?>
      
		<a href="http://www.cibanco.com/" target="blank"><img src="https://cdn.marcomares.com.mx//2020/05/BannerEstrategias-MarcoMares.jpg"></a>
		<?php echo do_shortcode('[table id=1 /]') ?>
      <?php
    // ACABA LOOP
    echo $args['after_widget'];
  }

  // Widget Backend 
  public function form($instance)
  {
    
    // Widget admin form
?>
    <p>
      <label>CIBanco activo</label>
      
    </p>
<?php
  }

  // Updating widget replacing old instances with new
  public function update($new_instance, $old_instance)
  {
    $instance = array();
    
    return $instance;
  }

  // Class pinno_cibanco ends here
}


// Register and load the widget
function load_cibanco()
{
  register_widget('pinno_cibanco');
}
add_action('widgets_init', 'load_cibanco');

?>