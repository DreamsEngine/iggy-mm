<?php
/**
 * Plugin Name: Home Feat Widget
*/

add_action('widgets_init', 'pinno_load_home_feat_widget');

function pinno_load_home_feat_widget() {
  register_widget('pinno_home_feat_widget');
}

class pinno_home_feat_widget extends WP_Widget { 
     
    // class constructor
	public function __construct() {
      $widget_ops = array( 
          'classname' => 'pinno_home_feat_widget',
          'description' => 'A widget that displays the destacate posts',
      );
      parent::__construct( 'pinno_home_feat_widget', 'IT0 - Home Feat Widget', $widget_ops);
    }

    public function widget( $args, $instance ) {  
        global $first_post;
        $first_post = get_first_post();
    ?>

<section id="pinno_home" class="pinno-widget-home left relative">
    <div class="pinno-main-box">
        <div class="channel__content">

            <div class="channel__cards">

            <?php
				//Loop Arguments
				$module_two_articles_args = array(
					'no_found_rows' => true,
					'update_post_meta_cache' => false,
					'update_post_term_cache' => false,
					'posts_per_page' => 5,
					'ignore_sticky_posts' => true,
					'post_type' => array('post'),
					'meta_query' => array(
						'relation' => 'OR',
						array(
							'key' => 'den_sidebarCheck',
							'compare' => 'NOT EXISTS'
						),
						array(
							'key' => 'den_sidebarCheck',
							'compare' => '!=',
							'value' => '1'
						),
					)

                );

                $module_two_articles = new WP_Query($module_two_articles_args);
                if ($module_two_articles->have_posts()) : ?>  
                <?php $counter = 1; while ($module_two_articles->have_posts()) : $module_two_articles->the_post(); ?>
                  <?php if ($counter == 1): ?>
                    <div class="card card--large">
  <a class="preview" href="<?php the_permalink(); ?>" aria-label="<?php the_title(); ?>" data-ga-track="Cover Story - blog - Position <?php echo $counter; ?> - <?php the_title(); ?>"
    >
    <div class="preview__overflow-wrapper">
    <?php $featured_img_url = get_the_post_thumbnail_url( null, 'full' );  ?>
      <div class="preview__image" style="background-image: url(<?php echo $featured_img_url; ?>)"></div>
    </div>
    <h3 class="preview__eyebrows"><span><?php category_link(); ?></span></h3>
    <div class="card--large__color-bar"></div
  ></a>
  <div class="card--large__title">
    <a
      class="headlink h1--dense"
      href="<?php the_permalink(); ?>"
      data-ga-track="Cover Story - blog - Position <?php echo $counter; ?> - <?php the_title(); ?>"
      ><?php the_title(); ?></a
    >
  </div>
  <p class="byline-stats byline-stats--large"><span class="byline"></span></p>
  <div class="byline__author-group">
    <div class="byline__author">
      <span class="byline__by">Por</span
      >
      <a class="byline__author-name" href="<?php echo get_the_author_meta('user_url'); ?>"
        data-ga-track="Cover Story - Position <?php echo $counter; ?> - <?php echo get_the_author_meta('display_name'); ?>"
        ><?php the_author_meta('display_name'); ?>
        </a>
      <!-- <span class="byline__author-type">Forbes Staff</span> -->
    </div>
  </div>
  <p></p>
  <h2 class="card__description">
  <?php echo wp_trim_words( get_the_excerpt(), 20, '...' ); ?>.
  </h2>
</div>
<div class="channel__column">

              <?php endif; ?>

              <?php if($counter >= 2 && $counter <= 5): ?>
                <?php $featured_img_url = get_the_post_thumbnail_url( null, 'full' );  ?>
                        <div class="card card--small">
                            <a class="preview" href="<?php the_permalink(); ?>" aria-label="The World’s Most Valuable  Esports Companies" data-ga-track="Homepage <?php get_the_category(); ?> - <?php get_the_title(); ?>">
                            <div class="show-img" background-image="<?php echo $featured_img_url; ?>" enhanced="<?php echo $featured_img_url; ?>" style="background-image: url(<?php echo $featured_img_url; ?>);">
                            </div>
                            </a>
                            <h2 class="card__fl-logo"><span><?php category_link(); ?></span></h2>
                            <a class="headlink" href="<?php the_permalink(); ?>" data-ga-track="Homepage <?php get_the_category(); ?> - <?php get_the_title(); ?>">
                                <h3 class="h3--dense"><?php the_title(); ?></h3>
                            </a>
                        <p class="body--dense list--description">  <?php echo wp_trim_words( get_the_excerpt(), 20, '...' ); ?></p>
                        </div>
              <?php endif; ?>

            <?php  $counter++; ?>
            <?php endwhile; endif; ?>
            </div>

              </div>
           <div class="channel__sidebar channel__sidebar--pop-picks pop-picks pop-picks--pop-active">
           <div class="pop-picks__nav"> 
              
              <button id="pick-popular" class="pop-picks__nav-btn pop-picks__nav-btn--popular pop-picks__nav-btn--active">
                <span>Últimas noticias</span>
                <div class="pop-picks__flipper">
                  <div class="pop-picks__square pop-picks__square--left"></div>
                  <div class="pop-picks__square pop-picks__square--right"></div>
                </div>
              </button>

              <button id="pick-editors" class="pop-picks__nav-btn pop-picks__nav-btn--ep">
                <span> Editors' Picks </span>
                <div class="pop-picks__flipper">
                    <div class="pop-picks__square pop-picks__square--left"></div>
                    <div class="pop-picks__square pop-picks__square--right"></div>
                </div>
              </button>
              
             

           </div> <!-- pop-picks__nav //-->
           
        

           <div id="popular-tab" class="pick-tab show">
               <?php
               //Loop Arguments
               $last_news_args = array(
                   'no_found_rows' => true,
                   'update_post_meta_cache' => false,
                   'update_post_term_cache' => false,
                   'posts_per_page' => 5,
                   'post_type' => 'post',
                   'ignore_sticky_posts' => true,
                   'post-type' => 'any'

               );

               //Query
               $last_news = new WP_Query($last_news_args); ?>

               <?php
               //Begin Loop
               if ($last_news->have_posts()) :
                   while ($last_news->have_posts()) :
                       $last_news->the_post(); ?>

                       <article class="f4_module--two__aside__article">
                           <figure class="<?php tagging_banner(); ?>">
                           <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                               <?php the_post_thumbnail('dreams-640x360', array('class' => 'f4_module-image--full lazyload', 'data-object-fit' => 'cover')); ?>
                           </a>
                           </figure>
                           <div class="f4_module--two__aside__article__metas">
                               <h6 class="f4_module--two__aside__article__title f4_title">
                                   <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                       <?php the_title(); ?>
                                   </a>
                               </h6>
                               <time class="f4_module--two__aside__article__time"><?php echo get_the_date(); ?></time>
                           </div>
                       </article>

                       <?php
                   endwhile;
               endif;
               //End Loop
               wp_reset_postdata(); ?>

           </div> <!-- pick-tab //-->

           <div id="editors-tab" class="pick-tab">
               <?php
               //Loop Arguments
               $module_two_last_args = array(

                   'no_found_rows' => true,
                   'update_post_meta_cache' => false,
                   'update_post_term_cache' => false,
                   'category_name' => "editors-picks",
                   'posts_per_page' => 5,
                   'post_type' => 'post',
                   'ignore_sticky_posts' => true,
                   'post-type' => 'any'

               );

               //Query
               $module_two_last = new WP_Query($module_two_last_args); ?>

               <?php
               //Begin Loop
               if ($module_two_last->have_posts()) :
                   while ($module_two_last->have_posts()) :
                       $module_two_last->the_post(); ?>

                       <article class="f4_module--two__aside__article">
                           <figure class="<?php tagging_banner(); ?>">
                           <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                               <?php the_post_thumbnail('dreams-640x360', array('class' => 'f4_module-image--full lazyload', 'data-object-fit' => 'cover')); ?>
                           </a>
                           </figure>
                           <div class="f4_module--two__aside__article__metas">
                               <h6 class="f4_module--two__aside__article__title f4_title">
                                   <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                       <?php the_title(); ?>
                                   </a>
                               </h6>
                               <time class="f4_module--two__aside__article__time"><?php echo get_the_date(); ?></time>
                           </div>
                       </article>

                       <?php
                   endwhile;
               endif;
               //End Loop
               wp_reset_postdata(); ?>

           </div> <!-- pick-tab //-->
       <!-- End Last -->

 

       <div class="pinno-widget-ad left relative" style="margin:0.75em auto 0;">
				<span class="pinno-ad-label">Publicidad</span>
         <div class="pinno-ad-serving" data-post-id="256601">
              <div id="div-gpt-ad-mm-default-box-a-256601"></div>
          </div>
        </div>
            
</div>
<!-- End Aside -->
          
            </div>
        </section> <!-- module-2 //-->

   <?php }    

   

}
