<?php

/**
 * Plugin Name: Text + 3 posts
 */

// Creating the widget 
class pinno_home_feat_category_title extends WP_Widget
{

  function __construct()
  {
    parent::__construct(

      // Base ID of your widget
      'pinno_home_feat_category_title',

      // Widget name will appear in UI
      __('Text + 3 posts', 'pinno_home_feat_category_title_domain'),

      // Widget description
      array('description' => __('Displays  3 posts in a row with a title floating left', 'pinno_home_feat_category_title_domain'),)
    );
  }

  // Creating widget front-end

  public function widget($args, $instance)
  {
    $title = apply_filters('widget_title', $instance['title']);
    $line_1 = apply_filters('widget_title', $instance['line_1']);
    $line_2 = apply_filters('widget_title', $instance['line_2']);
    $line_3 = apply_filters('widget_title', $instance['line_3']);

    // before and after widget arguments are defined by themes
    echo $args['before_widget'];
    if (!empty($title))
      //echo $args['before_title'] . $title . $args['after_title'];

      // This is where you run the code and display the output

      // EMPIEZA LOOP
      echo '<div class="pinno-widget-home-head">
    <h4 class="pinno-widget-home-title text3posts-title"><span class="pinno-widget-home-title">' . $instance['line_1'] . '</span>
    <span class="pinno-widget-home-title">' . $instance['line_3'] . '</span></h4>
    </div>';
    echo '<div class="pinno-widget-feat1-wrap left relative">';
    echo '<div class="pinno-widget-feat1-cont left relative">';
    $query1 = new WP_Query(array('posts_per_page' => 3, 'category_name' => $instance['title']));
    while ($query1->have_posts()) {
      $query1->the_post();

      echo '<a href="';
      echo the_permalink();
      echo '" rel="bookmark"> <div class="pinno-widget-feat1-bot-story left relative">';            
      echo '<div class="pinno-widget-feat1-bot-img left relative"><img width="400" height="240" src="';
      echo get_the_post_thumbnail_url();
      echo '"class="pinno-reg-img lazy wp-post-image" alt="" loading="lazy" ><img width="80" height="80" src="';
      echo get_the_post_thumbnail_url();
      echo '"class="pinno-mob-img lazy wp-post-image" alt="" loading="lazy" srcset="';
      echo get_the_post_thumbnail_url();
      echo '" sizes="(max-width: 80px) 100vw, 80px"></div><!--pinno-widget-feat1-bot-img-->
                    <div class="pinno-widget-feat1-bot-text left relative">';
                    echo '<div class="pinno-cat-date-wrap left relative">
                    <span class="pinno-cd-cat left relative">';
  $cat = get_the_category();
  //echo $cat[0]->cat_name;
  the_author();
  echo '</span></div><!--pinno-cat-date-wrap-->';
      echo '<h2>';
      echo the_title();
      echo '</h2>
                    </div><!--pinno-widget-feat1-bot-text-->
                  </div><!--pinno-widget-feat1-bot-story-->
                  </a>';
    }
    wp_reset_postdata();
    echo '</div><!--pinno-widget-feat1-cont-->';
    echo '</div><!--pinno-widget-feat1-wrap-->';

    // ACABA LOOP
    echo $args['after_widget'];
  }

  // Widget Backend 
  public function form($instance)
  {
    if (isset($instance['title'])) {
      $title = $instance['title'];
    } else {
      $title = __('New title', 'pinno_home_feat_category_title_domain');
    }
    if (isset($instance['line_1'])) {
      $line_1 = $instance['line_1'];
    } else {
      $line_1 = __('New line_1', 'pinno_home_feat_category_line_1_domain');
    }
    if (isset($instance['line_2'])) {
      $line_2 = $instance['line_2'];
    } else {
      $line_2 = __('New line_2', 'pinno_home_feat_category_line_2_domain');
    }
    if (isset($instance['line_3'])) {
      $line_3 = $instance['line_3'];
    } else {
      $line_3 = __('New line_3', 'pinno_home_feat_category_line_3_domain');
    }
    // Widget admin form
?>
    <p>
      <label for="<?php echo $this->get_field_id('line_1'); ?>"><?php _e('Line 1:'); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('line_1'); ?>" name="<?php echo $this->get_field_name('line_1'); ?>" type="text" value="<?php echo esc_attr($line_1); ?>" />
      <!--<label for="<?php echo $this->get_field_id('line_2'); ?>"><?php _e('Line 2:'); ?></label>
     <input class="widefat" id="<?php echo $this->get_field_id('line_2'); ?>" name="<?php echo $this->get_field_name('line_2'); ?>" type="text" value="<?php echo esc_attr($line_2); ?>" />-->
      <label for="<?php echo $this->get_field_id('line_3'); ?>"><?php _e('Line 3:'); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('line_3'); ?>" name="<?php echo $this->get_field_name('line_3'); ?>" type="text" value="<?php echo esc_attr($line_3); ?>" />
      <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Category:'); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
    </p>
<?php
  }

  // Updating widget replacing old instances with new
  public function update($new_instance, $old_instance)
  {
    $instance = array();
    $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
    $instance['line_1'] = (!empty($new_instance['line_1'])) ? strip_tags($new_instance['line_1']) : '';
    //$instance['line_2'] = (!empty($new_instance['line_2'])) ? strip_tags($new_instance['line_2']) : '';
    $instance['line_3'] = (!empty($new_instance['line_3'])) ? strip_tags($new_instance['line_3']) : '';
    return $instance;
  }

  // Class pinno_home_feat_category_title ends here
}


// Register and load the widget
function wpb_load_widget()
{
  register_widget('pinno_home_feat_category_title');
}
add_action('widgets_init', 'wpb_load_widget');

?>