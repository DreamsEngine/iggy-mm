jQuery(document).ready($ => {
  $sidebar_checkbox = $("#den_sidebarCheck");

  if ($sidebar_checkbox[0]) {
    set_imge_color($sidebar_checkbox[0].checked);

    $sidebar_checkbox.click(function() {
      set_imge_color(this.checked);
    });

    function set_imge_color(is_active) {
      if (is_active) {
        $('label[for="den_sidebarCheck"]')
          .css("filter", "grayscale(0%)")
          .css("opacity", "1.0");
      } else {
        $('label[for="den_sidebarCheck"]')
          .css("filter", "grayscale(100%)")
          .css("opacity", "0.3");
      }
      $('label[for="den_sidebarCheck"]').show();
    }
  }
});
