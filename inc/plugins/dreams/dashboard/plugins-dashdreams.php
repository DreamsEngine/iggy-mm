<?php
// Custom Styles and Effects
if (! function_exists('dreams_dashboard_css')) :
    function dreams_dashboard_css()
    {
        wp_register_style('dreams_dashboard', get_stylesheet_directory_uri() . '/inc/plugins/dreams/dashboard/css/dashboard.css', '', date("ymdHis", filemtime(get_stylesheet_directory() . '/inc/plugins/dreams/dashboard/css/dashboard.css')));

        wp_register_style('dreams_css', get_stylesheet_directory_uri() . '/inc/plugins/dreams/dashboard/css/dreams.css', '', date("ymdHis", filemtime(get_stylesheet_directory() . '/inc/plugins/dreams/dashboard/css/dreams.css')));


        wp_enqueue_style('dreams_dashboard');
        wp_enqueue_style('dreams_css');

    }
endif;

if (! function_exists('dreams_login_css')) :
    function dreams_login_css()
    {
        $template = get_stylesheet_directory_uri();
        echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"{$template}/inc/plugins/dreams/dashboard/css/dashboard.css\" />";
        echo "<style>
    html {
      background:none;
    }
  </style>";
    }
endif;


if (! function_exists('dreams_dashboard_js')) :
    function dreams_dashboard_js()
    {
        wp_enqueue_script(
            'dashboard-dreams-js',
            get_stylesheet_directory_uri() . '/inc/plugins/dreams/dashboard/js/dashboard.js',
            array('jquery'),
            date('ymdHis', filemtime(get_stylesheet_directory() . '/inc/plugins/dreams/dashboard/js/dashboard.js')),
            true
          ); 
    }
endif;


if (!function_exists('dreams_login_url')) :
    function dreams_login_url()
    {
        global $deCDN, $deSiteURL;
        return $deSiteURL;
    }
endif;

if (! function_exists('dreams_footer_admin')) :
    function dreams_footer_admin()
    {
        global $deCDN, $deSiteURL;
        echo "<span id=\"footer-thankyou\">Developed by <a href=\"$deSiteURL\" target=\"_blank\">Dreams Engine Agency</a></span>\n";
    }
endif;

//Add Mime Types
if(!function_exists('cc_mime_types')) :
    function cc_mime_types($mimes) {
     $mimes['svg'] = 'image/svg+xml';
     return $mimes;
    }
    add_filter('upload_mimes', 'cc_mime_types');
    endif;

//Execute PHP on Widgets
if (! function_exists('php_execute')) :
    function php_execute($html)
    {
        if (strpos($html, "<"."?php")!==false) {
            ob_start();
            eval("?".">".$html);
            $html=ob_get_contents();
            ob_end_clean();
        }
        return $html;
    }
endif;

//Remove Useless Titles of Widgets
if(! function_exists('remove_widget_title')) :
    function remove_widget_title( $widget_title ) {
        if ( substr ( $widget_title, 0, 1 ) == '!' )
            return;
        else
            return ( $widget_title );
    }
endif;


//Add Class to image content
if(!function_exists('add_image_class')):
    function add_image_class($content) {
        global $post;
        $pattern ="/<img(.*?)class=\"(.*?)\"(.*?)>/i";
        $replacement = '<img$1class="$2 lazyload"$3>';
        $content = preg_replace($pattern, $replacement, $content);
        return $content;
    }
endif;

