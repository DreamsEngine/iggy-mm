<?php
$GTM_CONTAINER_ID = 'GTM-PRCG9LG';
/**
 * Print amp-analytics.
 */
function print_component()
{
	printf(
		'<amp-analytics config="https://www.googletagmanager.com/amp.json?id=%s" data-credentials="include"></amp-analytics>',
		esc_attr($GTM_CONTAINER_ID)
	);
}
add_action('wp_footer', function () {
	if (function_exists('is_amp_endpoint') && is_amp_endpoint()) {
		print_component();
	}
});
// Classic mode.
add_filter('amp_post_template_data', function ($data) {
	$data['amp_component_scripts'] = array_merge($data['amp_component_scripts'], array(
		'amp-analytics' => true
	));
	return $data;
});
add_action('amp_post_template_footer', __NAMESPACE__ . '\print_component');
function gtm_init()
{
	?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PRCG9LG');</script>
<!-- End Google Tag Manager -->
<?php
}
add_action('wp_head', 'gtm_init', 10);