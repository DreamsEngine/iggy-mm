<?php
include_once(ABSPATH . 'wp-admin/includes/plugin.php');
if (is_plugin_active('amp/amp.php')) :

function remove_amp_parent_filters() { 
    remove_filter('amp_post_template_file', 'pinno_amp_set_custom_template', 10, 3);
}
add_action( 'after_setup_theme', 'remove_amp_parent_filters' );

    $pinno_enable_amp = get_option('pinno_amp'); 
    if ($pinno_enable_amp == "true") {
 
        add_filter('amp_post_template_file', 'new_pinno_amp_set_custom_template', 10, 3);

        function new_pinno_amp_set_custom_template($file, $type, $post)
        {
            if ('single' === $type) {
                $file = get_stylesheet_directory() . '/amp-single.php';
            }
            return $file;
            var_dump($file);
        }

        add_filter('amp_content_sanitizers', 'dreams_amp_add_ad_sanitizer', 10, 2);

        function dreams_amp_add_ad_sanitizer($sanitizer_classes, $post)
        {
            // require_once(dirname(__FILE__) . '/classes/ads.php');
            require_once INC_DIR . '/plugins/google/classes/amp.ads.php';

            $sanitizer_classes['DREAMS_AMP_Ad_Injection_Sanitizer'] = array(); // the array can be used to pass args to your sanitizer and accessed within the class via `$this->args`
            return $sanitizer_classes;
        }
    }
endif;