msgid ""
msgstr ""
"Project-Id-Version: MB User Profile\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-28 09:26+0700\n"
"PO-Revision-Date: 2019-06-28 09:29+0700\n"
"Last-Translator: \n"
"Language-Team: MetaBox.io <info@metabox.io>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 2.2.3\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;"
"_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;"
"esc_html__;esc_html_e;esc_html_x;esc_attr__;esc_attr_e;esc_attr_x\n"
"X-Poedit-Basepath: ..\n"
"X-Textdomain-Support: yes\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: css\n"
"X-Poedit-SearchPathExcluded-1: languages\n"
"X-Poedit-SearchPathExcluded-2: tests\n"

#: inc/class-mb-user-profile-user.php:79
msgid "Invalid user ID."
msgstr "ID người dùng không hợp lệ."

#: inc/class-mb-user-profile-user.php:89 inc/class-mb-user-profile-user.php:117
msgid "Passwords do not coincide."
msgstr "Mật khẩu không trùng khớp."

#: inc/class-mb-user-profile-user.php:109
msgid "Your username already exists."
msgstr "Tên người dùng của bạn đã tồn tại."

#: inc/class-mb-user-profile-user.php:113
msgid "Your email already exists."
msgstr "Địa chỉ email này đã tồn tại."

#: inc/forms/class-mb-user-profile-form-info.php:20
msgid "Please login to continue."
msgstr "Vui lòng đăng nhập để tiếp tục."

#: inc/forms/class-mb-user-profile-form-login.php:20
msgid "You are already logged in."
msgstr "Bạn đã đăng nhập."

#: inc/forms/class-mb-user-profile-form.php:104
msgid "Invalid form submit."
msgstr "Yêu cầu gửi form không hợp lệ."

#: inc/forms/class-mb-user-profile-form.php:150
msgid "Clear"
msgstr "Xóa"

#: inc/forms/class-mb-user-profile-form.php:151
msgid "Clear color"
msgstr "Xóa màu"

#: inc/forms/class-mb-user-profile-form.php:152
msgid "Default"
msgstr "Mặc định"

#: inc/forms/class-mb-user-profile-form.php:153
msgid "Select default color"
msgstr "Chọn màu mặc định"

#: inc/forms/class-mb-user-profile-form.php:154
msgid "Select Color"
msgstr "Chọn màu"

#: inc/forms/class-mb-user-profile-form.php:155
msgid "Color value"
msgstr "Mã màu"

#: inc/forms/class-mb-user-profile-form.php:170
msgid "Very weak"
msgstr "Rất yếu"

#: inc/forms/class-mb-user-profile-form.php:171
msgid "Weak"
msgstr "Yếu"

#: inc/forms/class-mb-user-profile-form.php:172
msgctxt "password strength"
msgid "Medium"
msgstr "Trung bình"

#: inc/forms/class-mb-user-profile-form.php:173
msgid "Strong"
msgstr "Mạnh"

#: inc/shortcodes/class-mb-user-profile-shortcode-info.php:36
#: mb-user-profile-fields.php:72
msgid "New Password"
msgstr "Mật khẩu mới"

#: inc/shortcodes/class-mb-user-profile-shortcode-info.php:37
#: inc/shortcodes/class-mb-user-profile-shortcode-register.php:39
#: mb-user-profile-fields.php:37 mb-user-profile-fields.php:79
msgid "Confirm Password"
msgstr "Xác nhận mật khẩu"

#: inc/shortcodes/class-mb-user-profile-shortcode-info.php:38
msgid "Submit"
msgstr "Gửi"

#: inc/shortcodes/class-mb-user-profile-shortcode-info.php:44
msgid "Your information has been successfully submitted. Thank you."
msgstr "Thông tin của bạn đã được gửi thành công. Cảm ơn bạn."

#: inc/shortcodes/class-mb-user-profile-shortcode-login.php:40
#: mb-user-profile-fields.php:51
msgid "Username or Email Address"
msgstr "Tên truy cập hoặc địa chỉ email"

#: inc/shortcodes/class-mb-user-profile-shortcode-login.php:41
#: inc/shortcodes/class-mb-user-profile-shortcode-register.php:38
#: mb-user-profile-fields.php:30 mb-user-profile-fields.php:57
msgid "Password"
msgstr "Mật khẩu"

#: inc/shortcodes/class-mb-user-profile-shortcode-login.php:42
msgid "Remember Me"
msgstr "Ghi nhớ"

#: inc/shortcodes/class-mb-user-profile-shortcode-login.php:43
msgid "Lost Password?"
msgstr "Quên mật khẩu?"

#: inc/shortcodes/class-mb-user-profile-shortcode-login.php:44
msgid "Log In"
msgstr "Đăng nhập"

#: inc/shortcodes/class-mb-user-profile-shortcode-login.php:56
msgid "You are now logged in."
msgstr "Bạn đã đăng nhập."

#: inc/shortcodes/class-mb-user-profile-shortcode-register.php:36
#: mb-user-profile-fields.php:18
msgid "Username"
msgstr "Tên đăng nhập"

#: inc/shortcodes/class-mb-user-profile-shortcode-register.php:37
#: mb-user-profile-fields.php:24
msgid "Email"
msgstr "Email"

#: inc/shortcodes/class-mb-user-profile-shortcode-register.php:40
msgid "Register"
msgstr "Đăng ký"

#: inc/shortcodes/class-mb-user-profile-shortcode-register.php:48
msgid "Your account has been created successfully."
msgstr "Tài khoản của bạn đã được tạo thành công."

#~ msgid "Current Color"
#~ msgstr "Màu đã chọn"

#~ msgid "Your account is already there"
#~ msgstr "Tài khoản của bạn đã hoạt động"

#~ msgid "First Name"
#~ msgstr "Tên"

#~ msgid "Last Name"
#~ msgstr "Họ"
