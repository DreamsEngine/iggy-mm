<?php
/*---[ Details ]---------------------------------------
Function 1.0
Author: Adrian Galvez G.
Contact: adrian@dreamsengine.io
-------------------------------------------------------*/

/*-----------------------------------------------------
[00] Global Variables
[01] Base & Framwork Options
[02] Load Javascript
[03] Plugins
[04] Custom Post Types and Taxonomies
[05] Meta Box Panel
-------------------------------------------------------*/

/* [00] Global Variables
-------------------------------------------------------*/
$themename = "M_Mares";
$prefix = "den_";
$denCDN = "https://cdn.devstate.de";
$denSiteURL = "https://dreamsengine.io";

/* Define Lib */
define('LIB_DIR', trailingslashit(get_stylesheet_directory(). '/lib'));

require_once LIB_DIR.'log.inc.php';

/* Define Includes */
define('INC_DIR', trailingslashit(get_stylesheet_directory(). '/inc'));

/* Define Metaboxes */
define('MB_DIR', trailingslashit(get_stylesheet_directory(). '/inc/plugins/meta-box'));
define('MB_ADDON_DIR', trailingslashit(get_stylesheet_directory(). '/inc/plugins/meta-box-plugins'));

/* Define Dreams Plugins*/
define( 'DREAMS_DIR', trailingslashit( get_stylesheet_directory(). '/inc/plugins/dreams' ) );
define( 'WIDGETS_DIR', trailingslashit( get_stylesheet_directory(). '/inc/widgets' ) );
define( 'WIDGETS_DIR2', trailingslashit( get_stylesheet_directory(). '/widgets' ) );



/* [01] Base & Framwork Options
-------------------------------------------------------*/
require_once INC_DIR .  'op/op-base.php';

/* [03] Dashboard & Framework Options
-------------------------------------------------------*/
require_once INC_DIR . "op/op-dashboard.php";

/* [04] Load JS$
-------------------------------------------------------*/
require_once INC_DIR .  'op/op-loader.php';

/* [05] Load JS$
-------------------------------------------------------*/
require_once INC_DIR .  'extras.php';

/* [06] Widgets
-------------------------------------------------------*/
require_once WIDGETS_DIR . 'widget-home-main.php';
require_once WIDGETS_DIR . 'widget-home-feat.php';
require_once WIDGETS_DIR . 'widget-home-video-sect.php';
require_once WIDGETS_DIR . 'widget-home-red-forbes.php';
require_once WIDGETS_DIR . 'widget-home-grids.php';
require_once WIDGETS_DIR . 'widget-text-3-posts.php';
require_once WIDGETS_DIR . 'widget-fullwidth-featured.php';
require_once WIDGETS_DIR . 'widget-flex2.php';
require_once WIDGETS_DIR . 'widget-flex3.php';
require_once WIDGETS_DIR . 'widget-flex4.php';
require_once WIDGETS_DIR . 'widget-author-sidebar.php';
require_once WIDGETS_DIR . 'widget-sidetab-flex.php';
require_once WIDGETS_DIR . 'widget-cibanco.php';

/* [07] Metaboxes
-------------------------------------------------------*/
require_once INC_DIR . 'op/op-metaboxes.php';

/**
 * Remove Custom Fields meta box
 */
//add_action( 'do_meta_boxes' , 'remove_post_custom_fields' );

function remove_post_custom_fields() {
  remove_meta_box( 'pinno-video-embed' , 'post' , 'normal' ); 
  remove_meta_box( 'pinno-featured-headline' , 'post' , 'normal' ); 
  remove_meta_box( 'pinno-photo-credit' , 'post' , 'normal' ); 
}

/* [09] New footer
-------------------------------------------------------*/

function footer_widget() {

	register_sidebar( array(
		'name'          => 'Footer videos',
		'id'            => 'footer_videos',
		'before_widget' => '<div class="center_foot">',
		'after_widget'  => '</div>'
	) );

}
add_action( 'widgets_init', 'footer_widget' );

function footer_widget_right() {

	register_sidebar( array(
		'name'          => 'Footer right',
		'id'            => 'footer_right',
		'before_widget' => '<div class="center_foot">',
		'after_widget'  => '</div>'
	) );

}
add_action( 'widgets_init', 'footer_widget_right' );

/* [09] New sidebars
-------------------------------------------------------*/

function history_sidebar() {
	register_sidebar( array(
    'name'          => 'History widget sidebar',
    'id'            => 'history-sidebar',
    'before_widget' => '<div class="pinno-feat1-list-wrap left relative ">',
    'after_widget'  => '</div>',
    'before_title'  => '<div>',
    'after_title'   => '</div>' 
  ) );
	}

  add_action( 'widgets_init', 'history_sidebar' );

  function block2_sidebar() {
    register_sidebar( array(
      'name'          => 'block2 widget sidebar',
      'id'            => 'block2-sidebar',
      'before_widget' => '<div class="pinno-feat1-list-wrap left relative ">',
      'after_widget'  => '</div>',
      'before_title'  => '<div>',
      'after_title'   => '</div>' 
    ) );
    }
  
    add_action( 'widgets_init', 'block2_sidebar' );

    function block3_sidebar() {
      register_sidebar( array(
        'name'          => 'block3 widget sidebar',
        'id'            => 'block3-sidebar',
        'before_widget' => '<div class="pinno-feat1-list-wrap left relative ">',
        'after_widget'  => '</div>',
        'before_title'  => '<div>',
        'after_title'   => '</div>' 
      ) );
      }
    
      add_action( 'widgets_init', 'block3_sidebar' );

  ?>