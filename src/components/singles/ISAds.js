/* eslint-disable no-console */
const createAds = () => {
  const firstLot = () => {
    const firstlots = document.querySelectorAll("#pinno-content-main");

    firstlots.forEach(flot => {
      console.info(`Creating Ads for Single View:\n`);
      console.info(`For flot: ${flot}`);

      const dataFirstArticle = flot.dataset.postId;
      const newFirstAdSlot = document.createElement("div");
      newFirstAdSlot.className = "pinno-ad-serving pinno-ad-center";
      newFirstAdSlot.setAttribute("data-post-id", dataFirstArticle);

      const newFirstAd = document.createElement("div");
      newFirstAd.id = `div-gpt-ad-mm-default-inread-${dataFirstArticle}`;

      const adFirstWrapper = document.createElement("div");
      adFirstWrapper.id = "pinno-post-bot-ad";
      adFirstWrapper.className = "left relative";

      const adFirstTitles = document.createElement("span");
      adFirstTitles.className = "pinno-ad-label";
      adFirstTitles.innerHTML = "PUBLICIDAD";

      adFirstWrapper.appendChild(adFirstTitles);
      newFirstAdSlot.appendChild(adFirstWrapper);

      newFirstAdSlot.appendChild(newFirstAd);

      const firstChild = flot.children[1];
      flot.insertBefore(newFirstAdSlot, firstChild);
    });
  };

  const secondLot = () => {
    const secondLots = document.querySelectorAll(".pinno-post-add-main");

    secondLots.forEach(seLot => {
      console.log(`For seLot: ${seLot}`);

      const dataSecondArticle = seLot.dataset.postId;
      const newSeAdSlot = document.createElement("div");
      newSeAdSlot.className = "pinno-ad-serving pinno-ad-center";
      newSeAdSlot.setAttribute("data-post-id", dataSecondArticle);

      const newSecondAd = document.createElement("div");
      newSecondAd.id = `div-gpt-ad-mm-default-inread-${dataSecondArticle}`;

      const adSecondWrapper = document.createElement("div");
      adSecondWrapper.id = "pinno-post-bot-ad";
      adSecondWrapper.className = "left relative";

      const adSecondTitles = document.createElement("span");
      adSecondTitles.className = "pinno-ad-label";
      adSecondTitles.innerHTML = "PUBLICIDAD";

      adSecondWrapper.appendChild(adSecondTitles);
      newSeAdSlot.appendChild(adSecondWrapper);

      newSeAdSlot.appendChild(newSecondAd);

      const secondChild = seLot.children[1];
      seLot.insertBefore(newSeAdSlot, secondChild);
    });
  };

  firstLot();
  secondLot();
  console.info(`Ads Creted`);
};

export default createAds;
