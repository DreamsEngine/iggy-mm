/* eslint-disable no-undef */
/* eslint-disable no-console */
// require("waypoints/lib/noframework.waypoints.min");

const headerFunction = () => {
  const topAd = document.querySelector("#firstAdd");
  const removeAd = document.getElementById(
    "div-gpt-ad-mm-default-top-b-88601"
  );
  const deckNormal = document.getElementById("breakStuck");

  const showNav = callback => {
    topAd.classList.remove("let-ad-remove");
    callback();
  };

  const stuck = new Waypoint({
    element: deckNormal,
    handler(direction) {
      if (direction === "up") {
        showNav(() => {
          topAd.classList.add("let-ad-show");
        });
      }
    },
    offset: -10
  });

  const breakAd = new Waypoint({
    element: removeAd,
    handler(direction) {
      if (direction === "down") {
        topAd.classList.add("let-ad-remove");
        topAd.classList.remove("let-ad-show");
      }
    },
    offset: 150
  });

  if (topAd) {
    stuck.enable();
    breakAd.enable();
  }
};

export default headerFunction;
