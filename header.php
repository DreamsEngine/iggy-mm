<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>" >
<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { if(get_option('pinno_favicon')) { ?><link rel="shortcut icon" href="<?php echo esc_url(get_option('pinno_favicon')); ?>" /><?php } } ?>
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php if ( is_single() ) { ?>
<meta property="og:type" content="article" />
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
 <?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
  <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'pinno-post-thumb' ); ?>
  <meta property="og:image" content="<?php echo esc_url( $thumb['0'] ); ?>" />
  <meta name="twitter:image" content="<?php echo esc_url( $thumb['0'] ); ?>" />
 <?php } ?>
<meta property="og:url" content="<?php the_permalink() ?>" />
<meta property="og:title" content="<?php the_title_attribute(); ?>" />
<meta property="og:description" content="<?php echo strip_tags(get_the_excerpt()); ?>" />
<meta name="twitter:card" content="summary">
<meta name="twitter:url" content="<?php the_permalink() ?>">
<meta name="twitter:title" content="<?php the_title_attribute(); ?>">
<meta name="twitter:description" content="<?php echo strip_tags(get_the_excerpt()); ?>">
<?php endwhile; endif; ?>
<?php } else { ?>
<meta property="og:description" content="<?php bloginfo('description'); ?>" />
<?php } ?>
<?php wp_head(); ?>
</head>
<body <?php body_class(''); ?>>
 <?php get_template_part('fly-menu'); ?>
 <div id="pinno-site" class="left relative">
  <div id="pinno-search-wrap">
   <div id="pinno-search-box">
    <?php get_search_form(); ?>
   </div><!--pinno-search-box-->
   <div class="pinno-search-but-wrap pinno-search-click">
    <span></span>
    <span></span>
   </div><!--pinno-search-but-wrap-->
  </div><!--pinno-search-wrap-->
  <?php if(get_option('pinno_wall_ad')) { ?>
   <div id="pinno-wallpaper">
    <?php if(get_option('pinno_wall_url')) { ?>
     <a href="<?php echo esc_url(get_option('pinno_wall_url')); ?>" class="pinno-wall-link" target="_blank"></a>
    <?php } ?>
   </div><!--pinno-wallpaper-->
  <?php } ?>
  <div id="pinno-site-wall" class="left relative">
   <div id="pinno-site-main" class="left relative">
   <header id="pinno-main-head-wrap" class="left relative">
    <?php $pinno_nav_layout = get_option('pinno_nav_layout'); if( $pinno_nav_layout == "1" ) { ?>
     <nav id="pinno-main-nav-wrap" class="left relative">
      <div id="pinno-main-nav-small" class="left relative">
       <div id="pinno-main-nav-small-cont" class="left">
        <div class="pinno-main-box">
         <div id="pinno-nav-small-wrap">
          <div class="pinno-nav-small-right-out left">
           <div class="pinno-nav-small-right-in">
            <div class="pinno-nav-small-cont left">
             <div class="pinno-nav-small-left-out right">
              <div id="pinno-nav-small-left" class="left relative">
               <div class="pinno-fly-but-wrap pinno-fly-but-click left relative">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
               </div><!--pinno-fly-but-wrap-->
              </div><!--pinno-nav-small-left-->
              <div class="pinno-nav-small-left-in">
               <div class="pinno-nav-small-mid left">
                <div class="pinno-nav-small-logo left relative">
                 <?php if(get_option('pinno_logo_nav')) { ?>
                  <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(get_option('pinno_logo_nav')); ?>" alt="<?php bloginfo( 'name' ); ?>" data-rjs="2" /></a>
                 <?php } else { ?>
                  <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logos/logo-nav.png" alt="

<?php bloginfo( 'name' ); ?>" data-rjs="2" /></a>
                 <?php } ?>
                 <?php if ( is_home() || is_front_page() ) { ?>
                  <h1 class="pinno-logo-title"><?php bloginfo( 'name' ); ?></h1>
                 <?php } else { ?>
                  <h2 class="pinno-logo-title"><?php bloginfo( 'name' ); ?></h2>
                 <?php } ?>
                </div><!--pinno-nav-small-logo-->
                <div class="pinno-nav-small-mid-right left">
                 <?php if (is_single()) { ?>
                  <div class="pinno-drop-nav-title left">
                   <h4><?php the_title(); ?></h4>
                  </div><!--pinno-drop-nav-title-->
                 <?php } ?>
                 <div class="pinno-nav-menu left">
                  <?php wp_nav_menu(array('theme_location' => 'main-menu')); ?>
                 </div><!--pinno-nav-menu-->
                </div><!--pinno-nav-small-mid-right-->
               </div><!--pinno-nav-small-mid-->
              </div><!--pinno-nav-small-left-in-->
             </div><!--pinno-nav-small-left-out-->
            </div><!--pinno-nav-small-cont-->
           </div><!--pinno-nav-small-right-in-->
           <div id="pinno-nav-small-right" class="right relative">
            <span class="pinno-nav-search-but fa fa-search fa-2 pinno-search-click"></span>
           </div><!--pinno-nav-small-right-->
          </div><!--pinno-nav-small-right-out-->
         </div><!--pinno-nav-small-wrap-->
        </div><!--pinno-main-box-->
	   </div><!--pinno-main-nav-small-cont-->
     
     <?php if(is_home() || is_front_page()) { ?>
	   <!-- add banner here //-->	  
	   <div id="firstAdd" class="banner-holder" style="display: none; visibility:hidden;">
	    <div id="pinno-leader-wrap">
	      <?php if(get_option('pinno_header_leader')) { ?>
	        <?php global $post; 
	   	      $pinno_post_layout = get_option('pinno_post_layout'); $pinno_post_temp = get_post_meta($post->ID, "pinno_post_template", true); 
	   	      if ( ( ! $pinno_post_temp && $pinno_post_layout == 'Template 5' && is_single() ) || ( ! $pinno_post_temp && $pinno_post_layout == 'Template 6' && is_single() ) || ( $pinno_post_temp == "global" && $pinno_post_layout == 'Template 5' && is_single() ) || ( $pinno_post_temp == "global" && $pinno_post_layout == 'Template 6' && is_single() ) || ( $pinno_post_temp == "temp5" && is_single() ) || ( $pinno_post_temp == "temp6" && is_single() ) ) { 
	   		  } else { ?>
	            <?php //$leader_ad = get_option('pinno_header_leader'); if ($leader_ad && ! is_404()) { echo html_entity_decode($leader_ad); } ?>
	          <?php } ?>
          <?php } ?>
	    </div> <!--pinno-leader-wrap-->   
	   </div> <!-- firstAdd banner-holder //--> 
     <!-- ends banner here //-->
    <?php } ?>

	  </div><!--pinno-main-nav-small-->
     </nav><!--pinno-main-nav-wrap-->
    <?php } else { ?>
     <nav id="pinno-main-nav-wrap" class="left relative">
      <div id="pinno-main-nav-top" class="left relative">
       <div class="pinno-main-box">
        <div id="pinno-nav-top-wrap" class="left relative">
         <div class="pinno-nav-top-right-out left relative">
          <div class="pinno-nav-top-right-in">
           <div class="pinno-nav-top-cont left relative">
            <div class="pinno-nav-top-left-out relative">
             <div class="pinno-nav-top-left">
              <div class="pinno-nav-soc-wrap">
               <?php if(get_option('pinno_facebook')) { ?>
                <a href="<?php echo esc_html(get_option('pinno_facebook')); ?>" target="_blank"><span class="pinno-nav-soc-but fa fa-facebook fa-2"></span></a>
               <?php } ?>
               <?php if(get_option('pinno_twitter')) { ?>
                <a href="<?php echo esc_html(get_option('pinno_twitter')); ?>" target="_blank"><span class="pinno-nav-soc-but fa fa-twitter fa-2"></span></a>
               <?php } ?>
               <?php if(get_option('pinno_instagram')) { ?>
                <a href="<?php echo esc_html(get_option('pinno_instagram')); ?>" target="_blank"><span class="pinno-nav-soc-but fa fa-instagram fa-2"></span></a>
               <?php } ?>
               <?php if(get_option('pinno_youtube')) { ?>
                <a href="<?php echo esc_html(get_option('pinno_youtube')); ?>" target="_blank"><span class="pinno-nav-soc-but fa fa-youtube-play fa-2"></span></a>
               <?php } ?>
              </div><!--pinno-nav-soc-wrap-->
              <div class="pinno-fly-but-wrap pinno-fly-but-click left relative">
               <span></span>
               <span></span>
               <span></span>
               <span></span>
              </div><!--pinno-fly-but-wrap-->
             </div><!--pinno-nav-top-left-->
             <div class="pinno-nav-top-left-in">
              <div class="pinno-nav-top-mid left relative" itemscope itemtype="http://schema.org/Organization">
               <?php if(get_option('pinno_logo')) { ?>
                <a class="pinno-nav-logo-reg" itemprop="url" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img itemprop="logo" src="<?php echo esc_url(get_option('pinno_logo')); ?>" alt="<?php bloginfo( 'name' ); ?>" data-rjs="2" /></a>
               <?php } else { ?>
                <a class="pinno-nav-logo-reg" itemprop="url" href="<?php echo esc_url( home_url

( '/' ) ); ?>"><img itemprop="logo" src="<?php echo get_template_directory_uri(); ?>/images/logos/logo-large.png" alt="<?php bloginfo( 'name' ); ?>" data-rjs="2" /></a>
               <?php } ?>
               <?php if(get_option('pinno_logo_nav')) { ?>
                <a class="pinno-nav-logo-small" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(get_option('pinno_logo_nav')); ?>" alt="<?php bloginfo( 'name' ); ?>" data-rjs="2" /></a>
               <?php } else { ?>
                <a class="pinno-nav-logo-small" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logos/logo-nav.png" alt="<?php bloginfo( 'name' ); ?>" data-rjs="2" /></a>
               <?php } ?>
               <?php if ( is_home() || is_front_page() ) { ?>
                <h1 class="pinno-logo-title"><?php bloginfo( 'name' ); ?></h1>
               <?php } else { ?>
                <h2 class="pinno-logo-title"><?php bloginfo( 'name' ); ?></h2>
               <?php } ?>
               <?php if (is_single()) { ?>
                <div class="pinno-drop-nav-title left">
                 <h4><?php the_title(); ?></h4>
                </div><!--pinno-drop-nav-title-->
               <?php } ?>
              </div><!--pinno-nav-top-mid-->
             </div><!--pinno-nav-top-left-in-->
            </div><!--pinno-nav-top-left-out-->
           </div><!--pinno-nav-top-cont-->
          </div><!--pinno-nav-top-right-in-->
          <div class="pinno-nav-top-right">
           <?php if ( class_exists( 'WooCommerce' ) ) { ?>
            <div class="pinno-woo-cart-wrap">
             <a class="pinno-woo-cart" href="<?php echo wc_get_cart_url(); ?>" title="<?php esc_html_e( 'View your shopping cart', 'iggy-type-0' ); ?>"><span class="pinno-woo-cart-num"><?php echo WC()->cart->get_cart_contents_count(); ?></span></a><span class="pinno-woo-cart-icon fa fa-shopping-cart" aria-hidden="true"></span>
            </div><!--pinno-woo-cart-wrap-->
           <?php } ?>
           <span class="pinno-nav-search-but fa fa-search fa-2 pinno-search-click"></span>
          </div><!--pinno-nav-top-right-->
         </div><!--pinno-nav-top-right-out-->
        </div><!--pinno-nav-top-wrap-->
       </div><!--pinno-main-box-->
      </div><!--pinno-main-nav-top-->
      <div id="pinno-main-nav-bot" class="left relative">
       <div id="pinno-main-nav-bot-cont" class="left">
        <div class="pinno-main-box">
         <div id="pinno-nav-bot-wrap" class="left">
          <div class="pinno-nav-bot-right-out left">
           <div class="pinno-nav-bot-right-in">
            <div class="pinno-nav-bot-cont left">
             <div class="pinno-nav-bot-left-out">
              <div class="pinno-nav-bot-left left relative">
               <div class="pinno-fly-but-wrap pinno-fly-but-click left relative">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
               </div><!--pinno-fly-but-wrap-->
              </div><!--pinno-nav-bot-left-->
              <div class="pinno-nav-bot-left-in">
               <div class="pinno-nav-menu left">
                <?php wp_nav_menu(array('theme_location' => 'main-menu')); ?>
               </div><!--pinno-nav-menu-->
              </div><!--pinno-nav-bot-left-in-->
             </div><!--pinno-nav-bot-left-out-->
            </div><!--pinno-nav-bot-cont-->
           </div><!--pinno-nav-bot-right-in-->
           <div class="pinno-nav-bot-right left relative">
            <span class="pinno-nav-search-but fa fa-search fa-2 pinno-search-click"></span>
           </div><!--pinno-nav-bot-right-->
          </div><!--pinno-nav-bot-right-out-->
         </div><!--pinno-nav-bot-wrap-->
        </div><!--pinno-main-nav-bot-cont-->
       </div><!--pinno-main-box-->
      </div><!--pinno-main-nav-bot-->
     </nav><!--pinno-main-nav-wrap-->
	<?php } ?>
	
	

   </header><!--pinno-main-head-wrap-->
           
   <?php //if(!is_front_page()) { ?>
	   <!-- add banner here //-->	  
	   <div class="banner-holder ok">
	    <div id="pinno-leader-wrap">
	      <?php if(get_option('pinno_header_leader')) { ?>
	        <?php global $post; 
             $pinno_post_layout = get_option('pinno_post_layout'); 
             $pinno_post_temp = get_post_meta($post->ID, "pinno_post_template", true); 
	   	      //if ( ( ! $pinno_post_temp && $pinno_post_layout == 'Template 5' && is_single() ) || ( ! $pinno_post_temp && $pinno_post_layout == 'Template 6' && is_single() ) || ( $pinno_post_temp == "global" && $pinno_post_layout == 'Template 5' && is_single() ) || ( $pinno_post_temp == "global" && $pinno_post_layout == 'Template 6' && is_single() ) || ( $pinno_post_temp == "temp5" && is_single() ) || ( $pinno_post_temp == "temp6" && is_single() ) ) { 
	   		  //} else { ?>
              <?php $leader_ad = get_option('pinno_header_leader'); 
              if ($leader_ad && ! is_404()) { echo html_entity_decode($leader_ad); } ?>
	          <?php //} ?>
          <?php } ?>
	    </div> <!--pinno-leader-wrap-->   
	   </div> <!-- firstAdd banner-holder //--> 
     <!-- ends banner here //-->
    <?php //} ?>
    
   <div id="pinno-main-body-wrap" class="left relative">
  <div id="breakStuck"></div> <!-- breakStuckAd //-->
