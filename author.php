<?php get_header(); ?>
<?php global $author; $userdata = get_userdata($author); ?>
<div class="pinno-main-blog-wrap left relative">
	<div class="pinno-main-box">
		<div class="pinno-main-blog-cont left relative">
			<div id="pinno-author-page-top" class="left relative">
				<div class="pinno-author-top-out right relative">
					<div id="pinno-author-top-left" class="left relative">
						<?php echo get_avatar( $userdata->user_email, '200' ); ?>
					</div><!--pinno-author-top-left-->
					<div class="pinno-author-top-in">
						<div id="pinno-author-top-right" class="left relative">
							<h1 class="pinno-author-top-head left"><?php echo esc_html( $userdata->display_name ); ?></h1>
							<span class="pinno-author-page-desc left relative"><?php echo wp_kses_post( $userdata->description ); ?></span>
							<ul class="pinno-author-page-list left relative">
								<?php $pinno_email = get_option('pinno_author_email'); if ($pinno_email == "true") { ?>
									<a href="mailto:<?php echo esc_html($userdata->user_email); ?>"><li class="fa fa-envelope-o fa-2"></li></a>
								<?php } ?>
								<?php $authordesc = $userdata->facebook; if ( ! empty ( $authordesc ) ) { ?>
									<a href="<?php echo esc_url( $userdata->facebook); ?>" alt="Facebook" target="_blank"><li class="fa fa-facebook fa-2"></li></a>
								<?php } ?>
								<?php $authordesc = $userdata->twitter; if ( ! empty ( $authordesc ) ) { ?>
									<a href="<?php echo esc_url( $userdata->twitter); ?>" alt="Twitter" target="_blank"><li class="fa fa-twitter fa-2"></li></a>
								<?php } ?>
								<?php $authordesc = $userdata->pinterest; if ( ! empty ( $authordesc ) ) { ?>
									<a href="<?php echo esc_url( $userdata->pinterest); ?>" alt="Pinterest" target="_blank"><li class="fa fa-pinterest-p fa-2"></li></a>
								<?php } ?>
								<?php $authordesc = $userdata->instagram; if ( ! empty ( $authordesc ) ) { ?>
									<a href="<?php echo esc_url( $userdata->instagram); ?>" alt="Instagram" target="_blank"><li class="fa fa-instagram fa-2"></li></a>
								<?php } ?>
								<?php $authordesc = $userdata->googleplus; if ( ! empty ( $authordesc ) ) { ?>
									<a href="<?php echo esc_url( $userdata->googleplus); ?>" alt="Google Plus" target="_blank"><li class="fa fa-google-plus fa-2"></li></a>
								<?php } ?>
								<?php $authordesc = $userdata->linkedin; if ( ! empty ( $authordesc ) ) { ?>
									<a href="<?php echo esc_url( $userdata->linkedin); ?>" alt="LinkedIn" target="_blank"><li class="fa fa-linkedin fa-2"></li></a>
								<?php } ?>
							</ul>
						</div><!--pinno-author-top-right-->
					</div><!--pinno-author-top-in-->
				</div><!--pinno-author-top-out-->
			</div><!--pinno-author-page-top-->
			<div class="pinno-main-blog-out left relative">
				<div class="pinno-main-blog-in">
					<div class="pinno-main-blog-body left relative">
						<div class="pinno-widget-home-head">
							<h4 class="pinno-widget-home-title"><span class="pinno-widget-home-title"><?php esc_html_e( 'Stories By', 'iggy-type-0' ); ?> <?php echo esc_html( $userdata->display_name ); ?></span></h4>
						</div><!--pinno-widget-home-head-->
						<?php if(get_option('pinno_arch_layout') == '1' ) { ?>
							<ul class="pinno-blog-story-list-col left relative infinite-content">
								<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
									<li class="pinno-blog-story-col left relative infinite-post">
										<a href="<?php the_permalink(); ?>" rel="bookmark">
										<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
											<div class="pinno-blog-story-out relative">
												<div class="pinno-blog-story-img left relative">
													<?php the_post_thumbnail('pinno-mid-thumb', array( 'class' => 'pinno-reg-img lazy' )); ?>
													<?php the_post_thumbnail('pinno-small-thumb', array( 'class' => 'pinno-mob-img lazy' )); ?>
													<?php if ( has_post_format( 'video' )) { ?>
														<div class="pinno-vid-box-wrap pinno-vid-marg">
															<i class="fa fa-2 fa-play" aria-hidden="true"></i>
														</div><!--pinno-vid-box-wrap-->
													<?php } else if ( has_post_format( 'gallery' )) { ?>
														<div class="pinno-vid-box-wrap">
															<i class="fa fa-2 fa-camera" aria-hidden="true"></i>
														</div><!--pinno-vid-box-wrap-->
													<?php } ?>
												</div><!--pinno-blog-story-img-->
												<div class="pinno-blog-story-in">
													<div class="pinno-blog-story-text left relative">
														<div class="pinno-cat-date-wrap left relative">
															<?php if ( is_sticky() ) { ?>
																<span class="pinno-cd-cat left relative sticky"><?php esc_html_e( 'Sticky Post', 'iggy-type-0' ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
															<?php } else { ?>
																<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
															<?php } ?>
														</div><!--pinno-cat-date-wrap-->
														<h2><?php the_title(); ?></h2>
														<p><?php echo wp_trim_words( get_the_excerpt(), 26, '...' ); ?></p>
													</div><!--pinno-blog-story-text-->
												</div><!--pinno-blog-story-in-->
											</div><!--pinno-blog-story-out-->
										<?php } else { ?>
											<div class="pinno-blog-story-text left relative">
												<div class="pinno-cat-date-wrap left relative">
													<?php if ( is_sticky() ) { ?>
														<span class="pinno-cd-cat left relative sticky"><?php esc_html_e( 'Sticky Post', 'iggy-type-0' ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
													<?php } else { ?>
														<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
													<?php } ?>
												</div><!--pinno-cat-date-wrap-->
												<h2><?php the_title(); ?></h2>
												<p><?php echo wp_trim_words( get_the_excerpt(), 26, '...' ); ?></p>
											</div><!--pinno-blog-story-text-->
										<?php } ?>
										</a>
									</li><!--pinno-blog-story-wrap-->
								<?php endwhile; endif; ?>
							</ul>
						<?php } else { ?>
							<ul class="pinno-blog-story-list left relative infinite-content">
								<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
									<li class="pinno-blog-story-wrap left relative infinite-post">
										<a href="<?php the_permalink(); ?>" rel="bookmark">
										<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
											<div class="pinno-blog-story-out relative">
												<div class="pinno-blog-story-img left relative">
													<?php the_post_thumbnail('pinno-mid-thumb', array( 'class' => 'pinno-reg-img lazy' )); ?>
													<?php the_post_thumbnail('pinno-small-thumb', array( 'class' => 'pinno-mob-img lazy' )); ?>
													<?php if ( has_post_format( 'video' )) { ?>
														<div class="pinno-vid-box-wrap pinno-vid-marg">
															<i class="fa fa-2 fa-play" aria-hidden="true"></i>
														</div><!--pinno-vid-box-wrap-->
													<?php } else if ( has_post_format( 'gallery' )) { ?>
														<div class="pinno-vid-box-wrap">
															<i class="fa fa-2 fa-camera" aria-hidden="true"></i>
														</div><!--pinno-vid-box-wrap-->
													<?php } ?>
												</div><!--pinno-blog-story-img-->
												<div class="pinno-blog-story-in">
													<div class="pinno-blog-story-text left relative">
														<div class="pinno-cat-date-wrap left relative">
															<?php if ( is_sticky() ) { ?>
																<span class="pinno-cd-cat left relative sticky"><?php esc_html_e( 'Sticky Post', 'iggy-type-0' ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
															<?php } else { ?>
																<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
															<?php } ?>
														</div><!--pinno-cat-date-wrap-->
														<h2><?php the_title(); ?></h2>
														<p><?php echo wp_trim_words( get_the_excerpt(), 26, '...' ); ?></p>
													</div><!--pinno-blog-story-text-->
												</div><!--pinno-blog-story-in-->
											</div><!--pinno-blog-story-out-->
										<?php } else { ?>
											<div class="pinno-blog-story-text left relative">
												<div class="pinno-cat-date-wrap left relative">
													<?php if ( is_sticky() ) { ?>
														<span class="pinno-cd-cat left relative sticky"><?php esc_html_e( 'Sticky Post', 'iggy-type-0' ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
													<?php } else { ?>
														<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
													<?php } ?>
												</div><!--pinno-cat-date-wrap-->
												<h2><?php the_title(); ?></h2>
												<p><?php echo wp_trim_words( get_the_excerpt(), 26, '...' ); ?></p>
											</div><!--pinno-blog-story-text-->
										<?php } ?>
										</a>
									</li><!--pinno-blog-story-wrap-->
								<?php endwhile; endif; ?>
							</ul>
						<?php } ?>
						<div class="pinno-inf-more-wrap left relative">
							<?php $pinno_infinite_scroll = get_option('pinno_infinite_scroll'); if ($pinno_infinite_scroll == "true") { if (isset($pinno_infinite_scroll)) { ?>
								<a href="#" class="pinno-inf-more-but"><?php esc_html_e( 'More Posts', 'iggy-type-0' ); ?></a>
							<?php } } ?>
							<div class="pinno-nav-links">
								<?php if (function_exists("pagination")) { pagination($wp_query->max_num_pages); } ?>
							</div><!--pinno-nav-links-->
						</div><!--pinno-inf-more-wrap-->
					</div><!--pinno-main-blog-body-->
				</div><!--pinno-pinno-main-blog-in-->
				<?php get_sidebar(); ?>
			</div><!--pinno-pinno-main-blog-out-->
		</div><!--pinno-main-blog-cont-->
	</div><!--pinno-main-box-->
</div><!--pinno-main-blog-wrap-->
<?php get_footer(); ?>