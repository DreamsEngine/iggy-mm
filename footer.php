</div>
<!--pinno-main-body-wrap-->
<div class="foot-media">
	<div class="foot-media-block">
		<h5>Fortuna y Poder</h5>
		<a class="twitter-timeline" data-lang="es" data-width="300" data-height="250" data-theme="light" href="https://twitter.com/fortunaypoder">Tweets by marco_mares</a>
		<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
		<br>
		<h5>Marco Mares</h5>
		<a class="twitter-timeline" data-lang="es" data-width="300" data-height="250" data-theme="light" href="https://twitter.com/marco_mares">Tweets by marco_mares</a>
		<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
	</div>
	<div class="foot-media-block">
		<?php if (is_active_sidebar('footer_videos')) : ?>
			<?php dynamic_sidebar('footer_videos'); ?>
		<?php endif; ?>
	</div>
	<div class="foot-media-block">
	<?php if (is_active_sidebar('footer_right')) : ?>
			<?php dynamic_sidebar('footer_right'); ?>
		<?php endif; ?>
	</div>
</div>

<footer id="pinno-foot-wrap" class="left relative">


	<div id="pinno-foot-logo" class="left relative">
		<!--<h4>Revista del mes</h4>-->

	</div>
	<div class="bottom_footer align-items-center ">
		<div class="footer_block">
			<h4><a>secciones</a></h4>
			<ul class="footer_list">
				<?php wp_nav_menu(array('theme_location' => 'footer-menu')); ?>
			</ul>
			<div class="foot_sep"></div>
			<h4>
				<a>Columnas</a>
			</h4>
			<ul class="footer_list">
				<li><a href="https://marcomares.com.mx/author/gonzalez/">Carlos González</a></li>
				<li><a href="https://marcomares.com.mx/author/daniela-blancas/">Daniela Blancas</a></li>
				<li><a href="https://marcomares.com.mx/author/fernando/">Fernando Franco</a></li>
				<li><a href="https://marcomares.com.mx/author/siller/">Gabriela Siller Pagaza</a></li>
				<li><a href="https://marcomares.com.mx/author/erhardt/">Wolfgang Erhart</a></li>
			</ul>
			<div class="foot_sep"></div>
			<ul class="pinno-foot-soc-list left relative">
				<li>
					<h4>Síguenos</h4>
				</li>
				<?php if (get_option('pinno_facebook')) { ?>
					<li><a href="<?php echo esc_url(get_option('pinno_facebook')); ?>" target="_blank" class="fa fa-facebook fa-2"></a></li>
				<?php } ?>
				<?php if (get_option('pinno_twitter')) { ?>
					<li><a href="<?php echo esc_url(get_option('pinno_twitter')); ?>" target="_blank" class="fa fa-twitter fa-2"></a></li>
				<?php } ?>
				<?php if (get_option('pinno_pinterest')) { ?>
					<li><a href="<?php echo esc_url(get_option('pinno_pinterest')); ?>" target="_blank" class="fa fa-pinterest-p fa-2"></a></li>
				<?php } ?>
				<?php if (get_option('pinno_instagram')) { ?>
					<li><a href="<?php echo esc_url(get_option('pinno_instagram')); ?>" target="_blank" class="fa fa-instagram fa-2"></a></li>
				<?php } ?>
				<?php if (get_option('pinno_google')) { ?>
					<li><a href="<?php echo esc_url(get_option('pinno_google')); ?>" target="_blank" class="fa fa-google-plus fa-2"></a></li>
				<?php } ?>
				<?php if (get_option('pinno_youtube')) { ?>
					<li><a href="<?php echo esc_url(get_option('pinno_youtube')); ?>" target="_blank" class="fa fa-youtube-play fa-2"></a></li>
				<?php } ?>
				<?php if (get_option('pinno_linkedin')) { ?>
					<li><a href="<?php echo esc_url(get_option('pinno_linkedin')); ?>" target="_blank" class="fa fa-linkedin fa-2"></a></li>
				<?php } ?>
				<?php if (get_option('pinno_tumblr')) { ?>
					<li><a href="<?php echo esc_url(get_option('pinno_tumblr')); ?>" target="_blank" class="fa fa-tumblr fa-2"></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div id="pinno-foot-bot" class="left relative">
		<div class="pinno-main-box">
			<div id="pinno-foot-copy" class="left relative">
				<p><?php echo wp_kses_post(get_option('pinno_copyright')); ?></p>
			</div>
			<!--pinno-foot-copy-->
		</div>
		<!--pinno-main-box-->
	</div>
</footer>
</div>
<!--pinno-site-main-->
</div>
<!--pinno-site-wall-->
</div>
<!--pinno-site-->
<div class="pinno-fly-top back-to-top">
	<i class="fa fa-angle-up fa-3"></i>
</div>
<!--pinno-fly-top-->
<div class="pinno-fly-fade pinno-fly-but-click">
</div>
<!--pinno-fly-fade-->
<?php wp_footer(); ?>
</body>

</html>