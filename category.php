<?php get_header(); ?>
<div class="pinno-main-blog-wrap left relative">
	<div class="pinno-main-box">
		<div class="pinno-main-blog-cont left relative">
			<?php $pinno_feat_cat = get_option('pinno_feat_cat'); if ($pinno_feat_cat == "true") { if ( $paged < 2 ) { ?>
				<?php $pinno_cat_layout = get_option('pinno_cat_layout'); if( $pinno_cat_layout == "1" ) { ?>
					<section id="pinno-feat6-wrap" class="left relative">
						<?php global $do_not_duplicate; global $post; $current_category = single_cat_title("", false); $category_id = get_cat_ID($current_category); $cat_posts = new WP_Query(array( 'cat' => get_query_var('cat'), 'posts_per_page' => '1', 'ignore_sticky_posts'=> 1 )); while($cat_posts->have_posts()) : $cat_posts->the_post(); $do_not_duplicate[] = $post->ID; if (isset($do_not_duplicate)) { ?>
							<a href="<?php the_permalink(); ?>" rel="bookmark">
							<div id="pinno-feat6-main" class="left relative">
								<div id="pinno-feat6-img" class="right relative">
									<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
										<?php the_post_thumbnail('pinno-post-thumb', array( 'class' => 'pinno-reg-img' )); ?>
										<?php the_post_thumbnail('pinno-port-thumb', array( 'class' => 'pinno-mob-img' )); ?>
									<?php } ?>
								</div><!--pinno-feat6-img-->
								<div id="pinno-feat6-text">
									<h3 class="pinno-feat1-pop-head"><span class="pinno-feat1-pop-head"><?php single_cat_title(); ?></span></h3>
									<h2><?php the_title(); ?></h2>
									<p><?php echo wp_trim_words( get_the_excerpt(), 20, '...' ); ?></p>
								</div><!--pinno-feat6-text-->
							</div><!--pinno-feat6-main-->
							</a>
						<?php } endwhile; wp_reset_postdata(); ?>
					</section><!--pinno-feat6-wrap-->
				<?php } ?>
			<?php } } ?>
			<div class="pinno-main-blog-out left relative">
				<div class="pinno-main-blog-in">
					<div class="pinno-main-blog-body left relative tipo2">
					<h1 class="pinno-feat1-pop-head"><span class=""><?php single_cat_title(); ?></span></h1>
						<?php $pinno_feat_cat = get_option('pinno_feat_cat'); if ($pinno_feat_cat == "true") { if ( $paged < 2 ) { ?>
							<?php $pinno_cat_layout = get_option('pinno_cat_layout'); if( $pinno_cat_layout == "0" ) { ?>
								<div id="pinno-cat-feat-wrap" class="left relative">
									<div class="pinno-widget-feat2-left left relative pinno-widget-feat2-left-alt">
										<?php global $do_not_duplicate; global $post; $current_category = single_cat_title("", false); $category_id = get_cat_ID($current_category); $cat_posts = new WP_Query(array( 'cat' => get_query_var('cat'), 'posts_per_page' => '1', 'ignore_sticky_posts'=> 1 )); while($cat_posts->have_posts()) : $cat_posts->the_post(); $do_not_duplicate[] = $post->ID; if (isset($do_not_duplicate)) { ?>
											<a href="<?php the_permalink(); ?>" rel="bookmark">
											<div class="pinno-widget-feat2-left-cont left relative">
												<div class="pinno-feat1-feat-img left relative">
													<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
														<?php the_post_thumbnail('pinno-port-thumb'); ?>
													<?php } ?>
													<?php if ( has_post_format( 'video' )) { ?>
														<div class="pinno-vid-box-wrap pinno-vid-marg">
															<i class="fa fa-2 fa-play" aria-hidden="true"></i>
														</div><!--pinno-vid-box-wrap-->
													<?php } else if ( has_post_format( 'gallery' )) { ?>
														<div class="pinno-vid-box-wrap">
															<i class="fa fa-2 fa-camera" aria-hidden="true"></i>
														</div><!--pinno-vid-box-wrap-->
													<?php } ?>
												</div><!--pinno-feat1-feat-img-->
												<div class="pinno-feat1-feat-text left relative">
													<div class="pinno-cat-date-wrap left relative">
														<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); if (isset($category)) { echo esc_html( $category[0]->cat_name ); } ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
													</div><!--pinno-cat-date-wrap-->
													<?php if(get_post_meta($post->ID, "pinno_featured_headline", true)): ?>
														<h2><?php echo esc_html(get_post_meta($post->ID, "pinno_featured_headline", true)); ?></h2>
													<?php else: ?>
														<h2 class="pinno-stand-title"><?php the_title(); ?></h2>
													<?php endif; ?>
													<p><?php echo wp_trim_words( get_the_excerpt(), 20, '...' ); ?></p>
												</div><!--pinno-feat1-feat-text-->
											</div><!--pinno-widget-feat2-left-cont-->
											</a>
										<?php } endwhile; wp_reset_postdata(); ?>
									</div><!--pinno-widget-feat2-left-->
									
								</div><!--pinno-cat-feat-wrap-->
							<?php } ?>
						<?php } } ?>
						<?php if(get_option('pinno_arch_layout') == '1' ) { ?>
							<div id="pinno-blog-arch-col-wrap" class="left relative pedro">
							<ul class="pinno-blog-story-list-col left relative infinite-content">
								<?php global $do_not_duplicate; if (isset($do_not_duplicate)) { ?>
									<?php if (have_posts()) : while (have_posts()) : the_post(); if (in_array($post->ID, $do_not_duplicate)) continue; ?>
										<li class="pinno-blog-story-col left relative infinite-post">
											<a href="<?php the_permalink(); ?>" rel="bookmark">
											<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
												<div class="pinno-blog-story-out relative">
													<div class="pinno-blog-story-img left relative">
														<?php the_post_thumbnail('pinno-mid-thumb', array( 'class' => 'pinno-reg-img lazy' )); ?>
														<?php the_post_thumbnail('pinno-small-thumb', array( 'class' => 'pinno-mob-img lazy' )); ?>
														<?php if ( has_post_format( 'video' )) { ?>
															<div class="pinno-vid-box-wrap pinno-vid-marg">
																<i class="fa fa-2 fa-play" aria-hidden="true"></i>
															</div><!--pinno-vid-box-wrap-->
														<?php } else if ( has_post_format( 'gallery' )) { ?>
															<div class="pinno-vid-box-wrap">
																<i class="fa fa-2 fa-camera" aria-hidden="true"></i>
															</div><!--pinno-vid-box-wrap-->
														<?php } ?>
													</div><!--pinno-blog-story-img-->
													<div class="pinno-blog-story-in">
														<div class="pinno-blog-story-text left relative">
															<div class="pinno-cat-date-wrap left relative">
																<?php if ( is_sticky() ) { ?>
																	<span class="pinno-cd-cat left relative sticky"><?php esc_html_e( 'Sticky Post', 'iggy-type-0' ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
																<?php } else { ?>
																	<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); if (isset($category)) { echo esc_html( $category[0]->cat_name ); } ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
																<?php } ?>
															</div><!--pinno-cat-date-wrap-->
															<h2><?php the_title(); ?></h2>
															<p><?php echo wp_trim_words( get_the_excerpt(), 26, '...' ); ?></p>
														</div><!--pinno-blog-story-text-->
													</div><!--pinno-blog-story-in-->
												</div><!--pinno-blog-story-out-->
											<?php } else { ?>
												<div class="pinno-blog-story-text left relative">
													<div class="pinno-cat-date-wrap left relative">
														<?php if ( is_sticky() ) { ?>
															<span class="pinno-cd-cat left relative sticky"><?php esc_html_e( 'Sticky Post', 'iggy-type-0' ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
														<?php } else { ?>
															<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); if (isset($category)) { echo esc_html( $category[0]->cat_name ); } ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
														<?php } ?>
													</div><!--pinno-cat-date-wrap-->
													<h2><?php the_title(); ?></h2>
													<p><?php echo wp_trim_words( get_the_excerpt(), 26, '...' ); ?></p>
												</div><!--pinno-blog-story-text-->
											<?php } ?>
											</a>
										</li><!--pinno-blog-story-wrap-->
									<?php endwhile; endif; ?>
								<?php } else { ?>
									<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
										<li class="pinno-blog-story-col left relative infinite-post">
											<a href="<?php the_permalink(); ?>" rel="bookmark">
											<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
												<div class="pinno-blog-story-out relative">
													<div class="pinno-blog-story-img left relative">
														<?php the_post_thumbnail('pinno-mid-thumb', array( 'class' => 'pinno-reg-img lazy' )); ?>
														<?php the_post_thumbnail('pinno-small-thumb', array( 'class' => 'pinno-mob-img lazy' )); ?>
														<?php if ( has_post_format( 'video' )) { ?>
															<div class="pinno-vid-box-wrap pinno-vid-marg">
																<i class="fa fa-2 fa-play" aria-hidden="true"></i>
															</div><!--pinno-vid-box-wrap-->
														<?php } else if ( has_post_format( 'gallery' )) { ?>
															<div class="pinno-vid-box-wrap">
																<i class="fa fa-2 fa-camera" aria-hidden="true"></i>
															</div><!--pinno-vid-box-wrap-->
														<?php } ?>
													</div><!--pinno-blog-story-img-->
													<div class="pinno-blog-story-in">
														<div class="pinno-blog-story-text left relative">
															<div class="pinno-cat-date-wrap left relative">
																<?php if ( is_sticky() ) { ?>
																	<span class="pinno-cd-cat left relative sticky"><?php esc_html_e( 'Sticky Post', 'iggy-type-0' ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
																<?php } else { ?>
																	<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); if (isset($category)) { echo esc_html( $category[0]->cat_name ); } ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
																<?php } ?>
															</div><!--pinno-cat-date-wrap-->
															<h2><?php the_title(); ?></h2>
															<p><?php echo wp_trim_words( get_the_excerpt(), 26, '...' ); ?></p>
														</div><!--pinno-blog-story-text-->
													</div><!--pinno-blog-story-in-->
												</div><!--pinno-blog-story-out-->
											<?php } else { ?>
												<div class="pinno-blog-story-text left relative">
													<div class="pinno-cat-date-wrap left relative">
														<?php if ( is_sticky() ) { ?>
															<span class="pinno-cd-cat left relative sticky"><?php esc_html_e( 'Sticky Post', 'iggy-type-0' ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
														<?php } else { ?>
															<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); if (isset($category)) { echo esc_html( $category[0]->cat_name ); } ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
														<?php } ?>
													</div><!--pinno-cat-date-wrap-->
													<h2><?php the_title(); ?></h2>
													<p><?php echo wp_trim_words( get_the_excerpt(), 26, '...' ); ?></p>
												</div><!--pinno-blog-story-text-->
											<?php } ?>
											</a>
										</li><!--pinno-blog-story-wrap-->
									<?php endwhile; endif; ?>
								<?php } ?>
							</ul>
							</div><!--pinno-blog-arch-col-wrap-->
						<?php } else { ?>
							<ul class="pinno-blog-story-list left relative infinite-content">
								<?php global $do_not_duplicate; if (isset($do_not_duplicate)) { ?>
									<?php if (have_posts()) : while (have_posts()) : the_post(); if (in_array($post->ID, $do_not_duplicate)) continue; ?>
										<li class="pinno-blog-story-wrap left relative infinite-post">
											<a href="<?php the_permalink(); ?>" rel="bookmark">
											<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
												<div class="pinno-blog-story-out relative">
													<div class="pinno-blog-story-img left relative">
														<?php the_post_thumbnail('pinno-mid-thumb', array( 'class' => 'pinno-reg-img lazy' )); ?>
														<?php the_post_thumbnail('pinno-small-thumb', array( 'class' => 'pinno-mob-img lazy' )); ?>
														<?php if ( has_post_format( 'video' )) { ?>
															<div class="pinno-vid-box-wrap pinno-vid-marg">
																<i class="fa fa-2 fa-play" aria-hidden="true"></i>
															</div><!--pinno-vid-box-wrap-->
														<?php } else if ( has_post_format( 'gallery' )) { ?>
															<div class="pinno-vid-box-wrap">
																<i class="fa fa-2 fa-camera" aria-hidden="true"></i>
															</div><!--pinno-vid-box-wrap-->
														<?php } ?>
													</div><!--pinno-blog-story-img-->
													<div class="pinno-blog-story-in">
														<div class="pinno-blog-story-text left relative">
															<div class="pinno-cat-date-wrap left relative">
																<?php if ( is_sticky() ) { ?>
																	<span class="pinno-cd-cat left relative sticky"><?php esc_html_e( 'Sticky Post', 'iggy-type-0' ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
																<?php } else { ?>
																	<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); if (isset($category)) { echo esc_html( $category[0]->cat_name ); } ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
																<?php } ?>
															</div><!--pinno-cat-date-wrap-->
															<h2><?php the_title(); ?></h2>
															<p><?php echo wp_trim_words( get_the_excerpt(), 26, '...' ); ?></p>
														</div><!--pinno-blog-story-text-->
													</div><!--pinno-blog-story-in-->
												</div><!--pinno-blog-story-out-->
											<?php } else { ?>
												<div class="pinno-blog-story-text left relative">
													<div class="pinno-cat-date-wrap left relative">
														<?php if ( is_sticky() ) { ?>
															<span class="pinno-cd-cat left relative sticky"><?php esc_html_e( 'Sticky Post', 'iggy-type-0' ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
														<?php } else { ?>
															<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); if (isset($category)) { echo esc_html( $category[0]->cat_name ); } ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
														<?php } ?>
													</div><!--pinno-cat-date-wrap-->
													<h2><?php the_title(); ?></h2>
													<p><?php echo wp_trim_words( get_the_excerpt(), 26, '...' ); ?></p>
												</div><!--pinno-blog-story-text-->
											<?php } ?>
											</a>
										</li><!--pinno-blog-story-wrap-->
									<?php endwhile; endif; ?>
								<?php } else { ?>
									<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
										<li class="pinno-blog-story-wrap left relative infinite-post">
											<a href="<?php the_permalink(); ?>" rel="bookmark">
											<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
												<div class="pinno-blog-story-out relative">
													<div class="pinno-blog-story-img left relative">
														<?php the_post_thumbnail('pinno-mid-thumb', array( 'class' => 'pinno-reg-img lazy' )); ?>
														<?php the_post_thumbnail('pinno-small-thumb', array( 'class' => 'pinno-mob-img lazy' )); ?>
														<?php if ( has_post_format( 'video' )) { ?>
															<div class="pinno-vid-box-wrap pinno-vid-marg">
																<i class="fa fa-2 fa-play" aria-hidden="true"></i>
															</div><!--pinno-vid-box-wrap-->
														<?php } else if ( has_post_format( 'gallery' )) { ?>
															<div class="pinno-vid-box-wrap">
																<i class="fa fa-2 fa-camera" aria-hidden="true"></i>
															</div><!--pinno-vid-box-wrap-->
														<?php } ?>
													</div><!--pinno-blog-story-img-->
													<div class="pinno-blog-story-in">
														<div class="pinno-blog-story-text left relative">
															<div class="pinno-cat-date-wrap left relative">
																<?php if ( is_sticky() ) { ?>
																	<span class="pinno-cd-cat left relative sticky"><?php esc_html_e( 'Sticky Post', 'iggy-type-0' ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
																<?php } else { ?>
																	<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); if (isset($category)) { echo esc_html( $category[0]->cat_name ); } ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
																<?php } ?>
															</div><!--pinno-cat-date-wrap-->
															<h2><?php the_title(); ?></h2>
															<p><?php echo wp_trim_words( get_the_excerpt(), 26, '...' ); ?></p>
														</div><!--pinno-blog-story-text-->
													</div><!--pinno-blog-story-in-->
												</div><!--pinno-blog-story-out-->
											<?php } else { ?>
												<div class="pinno-blog-story-text left relative">
													<div class="pinno-cat-date-wrap left relative">
														<?php if ( is_sticky() ) { ?>
															<span class="pinno-cd-cat left relative sticky"><?php esc_html_e( 'Sticky Post', 'iggy-type-0' ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
														<?php } else { ?>
															<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); if (isset($category)) { echo esc_html( $category[0]->cat_name ); } ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
														<?php } ?>
													</div><!--pinno-cat-date-wrap-->
													<h2><?php the_title(); ?></h2>
													<p><?php echo wp_trim_words( get_the_excerpt(), 26, '...' ); ?></p>
												</div><!--pinno-blog-story-text-->
											<?php } ?>
											</a>
										</li><!--pinno-blog-story-wrap-->
									<?php endwhile; endif; ?>
								<?php } ?>
							</ul>
						<?php } ?>
						<div class="pinno-inf-more-wrap left relative">
							<?php $pinno_infinite_scroll = get_option('pinno_infinite_scroll'); if ($pinno_infinite_scroll == "true") { if (isset($pinno_infinite_scroll)) { ?>
								<a href="#" class="pinno-inf-more-but"><?php esc_html_e( 'More Posts', 'iggy-type-0' ); ?></a>
							<?php } } ?>
							<div class="pinno-nav-links">
								<?php if (function_exists("pagination")) { pagination($wp_query->max_num_pages); } ?>
							</div><!--pinno-nav-links-->
						</div><!--pinno-inf-more-wrap-->
					</div><!--pinno-main-blog-body-->
				</div><!--pinno-pinno-main-blog-in-->
				<?php get_sidebar(); ?>
			</div><!--pinno-pinno-main-blog-out-->
		</div><!--pinno-main-blog-cont-->
	</div><!--pinno-main-box-->
</div><!--pinno-main-blog-wrap-->
<?php get_footer(); ?>