# Terreno de juego

[![Version](https://img.shields.io/badge/version-1.0-blue.svg?label=version)](https://semver.org)
[![DreamsEngine](https://img.shields.io/website/https/forbes.co)](https://forbes.co)

<!-- [![dependencies status](https://david-dm.org/DreamsEngine/fnw/status.svg)](https://david-dm.org/DreamsEngine/fnw) -->

[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgitlab.com%2FDreamsEngine%2Ffnw.svg?type=shield)](https://app.fossa.io/projects/git%2Bgitlab.com%2FDreamsEngine%2Ffnw?ref=badge_shield)

<!-- [![MIT license](http://img.shields.io/badge/license-MIT-green.svg)](http://opensource.org/licenses/MIT) -->

## Getting Started

Para emepzar este proyecto solo se requiere de Clonarlo

### Prerequisites

Por el momento no requiere ningun tipo de extras

### Installing

## Running the tests

### Break down into end to end tests

### And coding style tests

## Deployment

## Built With

- [WP](https://wordpress.org) - The web framework used

## Contributing

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

- **Adrian Galvez** - _Initial work_ - [LockeAG](https://github.com/LockeAG)

See also the list of [contributors](https://github.com/DreamsEngine/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments
