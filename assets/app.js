/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./app.js":
/*!****************!*\
  !*** ./app.js ***!
  \****************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _den = _interopRequireDefault(__webpack_require__(/*! ./components/plugins/den */ \"./components/plugins/den.js\"));\n\nvar _header = _interopRequireDefault(__webpack_require__(/*! ./components/header/header */ \"./components/header/header.js\"));\n\nvar _homeMainDes = _interopRequireDefault(__webpack_require__(/*! ./components/home/home-main-des */ \"./components/home/home-main-des.js\"));\n\nvar _ISAds = _interopRequireDefault(__webpack_require__(/*! ./components/singles/ISAds */ \"./components/singles/ISAds.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n/* eslint-disable no-console */\n// components js\n// import VideoGlider from \"./components/home/home-videos\";\n// run variables\nvar isHome = document.body.classList.contains(\"home\");\nvar isSingle = document.body.classList.contains(\"single\"); // baseline app styles\n\n__webpack_require__(/*! ./app/fonts/fonts.css */ \"./app/fonts/fonts.css\");\n\n__webpack_require__(/*! ./app/app.css */ \"./app/app.css\"); // variables styles\n\n\n__webpack_require__(\"./variables sync recursive \\\\.css$\"); // components styles\n\n\n__webpack_require__(\"./components sync recursive \\\\.css$\");\n\n(0, _den.default)();\n\nif (isHome === true) {\n  (0, _homeMainDes.default)();\n  (0, _header.default)(); // VideoGlider();\n}\n\nif (isSingle === true) {\n  (0, _ISAds.default)();\n  console.warn(\"exist\");\n} else {\n  console.warn(\"does not exist\");\n}\n\n//# sourceURL=webpack:///./app.js?");

/***/ }),

/***/ "./app/app.css":
/*!*********************!*\
  !*** ./app/app.css ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./app/app.css?");

/***/ }),

/***/ "./app/fonts/fonts.css":
/*!*****************************!*\
  !*** ./app/fonts/fonts.css ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./app/fonts/fonts.css?");

/***/ }),

/***/ "./components sync recursive \\.css$":
/*!********************************!*\
  !*** ./components sync \.css$ ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var map = {\n\t\"./header/header.css\": \"./components/header/header.css\",\n\t\"./home/home-feat.css\": \"./components/home/home-feat.css\",\n\t\"./home/home-grid.css\": \"./components/home/home-grid.css\",\n\t\"./home/home-main-des.css\": \"./components/home/home-main-des.css\",\n\t\"./home/home-main.css\": \"./components/home/home-main.css\",\n\t\"./home/home-red.css\": \"./components/home/home-red.css\",\n\t\"./home/home-videos.css\": \"./components/home/home-videos.css\",\n\t\"./plugins/ads.css\": \"./components/plugins/ads.css\",\n\t\"./sidebar/sidebar.css\": \"./components/sidebar/sidebar.css\"\n};\n\n\nfunction webpackContext(req) {\n\tvar id = webpackContextResolve(req);\n\treturn __webpack_require__(id);\n}\nfunction webpackContextResolve(req) {\n\tif(!__webpack_require__.o(map, req)) {\n\t\tvar e = new Error(\"Cannot find module '\" + req + \"'\");\n\t\te.code = 'MODULE_NOT_FOUND';\n\t\tthrow e;\n\t}\n\treturn map[req];\n}\nwebpackContext.keys = function webpackContextKeys() {\n\treturn Object.keys(map);\n};\nwebpackContext.resolve = webpackContextResolve;\nmodule.exports = webpackContext;\nwebpackContext.id = \"./components sync recursive \\\\.css$\";\n\n//# sourceURL=webpack:///./components_sync_\\.css$?");

/***/ }),

/***/ "./components/header/header.css":
/*!**************************************!*\
  !*** ./components/header/header.css ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./components/header/header.css?");

/***/ }),

/***/ "./components/header/header.js":
/*!*************************************!*\
  !*** ./components/header/header.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\nexports.default = void 0;\n\n/* eslint-disable no-undef */\n\n/* eslint-disable no-console */\n// require(\"waypoints/lib/noframework.waypoints.min\");\nvar headerFunction = function headerFunction() {\n  var topAd = document.querySelector(\"#firstAdd\");\n  var removeAd = document.getElementById(\"div-gpt-ad-mm-default-top-b-88601\");\n  var deckNormal = document.getElementById(\"breakStuck\");\n\n  var showNav = function showNav(callback) {\n    topAd.classList.remove(\"let-ad-remove\");\n    callback();\n  };\n\n  var stuck = new Waypoint({\n    element: deckNormal,\n    handler: function handler(direction) {\n      if (direction === \"up\") {\n        showNav(function () {\n          topAd.classList.add(\"let-ad-show\");\n        });\n      }\n    },\n    offset: -10\n  });\n  var breakAd = new Waypoint({\n    element: removeAd,\n    handler: function handler(direction) {\n      if (direction === \"down\") {\n        topAd.classList.add(\"let-ad-remove\");\n        topAd.classList.remove(\"let-ad-show\");\n      }\n    },\n    offset: 150\n  });\n\n  if (topAd) {\n    stuck.enable();\n    breakAd.enable();\n  }\n};\n\nvar _default = headerFunction;\nexports.default = _default;\n\n//# sourceURL=webpack:///./components/header/header.js?");

/***/ }),

/***/ "./components/home/home-feat.css":
/*!***************************************!*\
  !*** ./components/home/home-feat.css ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./components/home/home-feat.css?");

/***/ }),

/***/ "./components/home/home-grid.css":
/*!***************************************!*\
  !*** ./components/home/home-grid.css ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./components/home/home-grid.css?");

/***/ }),

/***/ "./components/home/home-main-des.css":
/*!*******************************************!*\
  !*** ./components/home/home-main-des.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./components/home/home-main-des.css?");

/***/ }),

/***/ "./components/home/home-main-des.js":
/*!******************************************!*\
  !*** ./components/home/home-main-des.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\nexports.default = void 0;\n\nvar sidebarMenuHome = function sidebarMenuHome() {\n  var popularBtn = document.getElementById(\"pick-popular\");\n  var pickBtn = document.getElementById(\"pick-editors\");\n  var popularTab = document.getElementById(\"popular-tab\");\n  var editorsTab = document.getElementById(\"editors-tab\");\n\n  if (popularBtn) {\n    pickBtn.addEventListener(\"click\", function () {\n      popularBtn.classList.remove(\"pop-picks__nav-btn--active\");\n      pickBtn.classList.add(\"pop-picks__nav-btn--active\");\n      popularTab.classList.remove(\"show\");\n      editorsTab.classList.add(\"show\");\n    });\n    popularBtn.addEventListener(\"click\", function () {\n      pickBtn.classList.remove(\"pop-picks__nav-btn--active\");\n      popularBtn.classList.add(\"pop-picks__nav-btn--active\");\n      editorsTab.classList.remove(\"show\");\n      popularTab.classList.add(\"show\");\n    });\n  }\n\n  var getIdYoutube = function getIdYoutube(url) {\n    var regExp = /^.*((youtu.be\\/)|(v\\/)|(\\/u\\/\\w\\/)|(embed\\/)|(watch\\?))\\??v?=?([^#]*).*/;\n    var match = url.match(regExp);\n    return match && match[7].length === 11 ? match[7] : false;\n  };\n\n  var showHomeAsideVideo = function showHomeAsideVideo() {\n    var videoBtn = document.getElementById(\"homeVideoBtn\");\n\n    if (document.body.contains(videoBtn)) {\n      videoBtn.addEventListener(\"click\", function () {\n        window.DreamsBlockeshowModuleOneType5VideodRefresh = true;\n        var holder = document.querySelector(\".home-aside-video\");\n        var videoUrl = videoBtn.getAttribute(\"data-videourl\");\n        var idVideo = getIdYoutube(videoUrl);\n        var iframe = document.createElement(\"iframe\");\n        iframe.src = \"https://www.youtube.com/embed/\".concat(idVideo.replace(/ /g, \"\"), \"?enablejsapi=1&autoplay=1\");\n        iframe.setAttribute(\"allow\", \"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture;\");\n        holder.innerHTML = iframe.outerHTML;\n      });\n    }\n  };\n\n  showHomeAsideVideo();\n};\n\nvar _default = sidebarMenuHome;\nexports.default = _default;\n\n//# sourceURL=webpack:///./components/home/home-main-des.js?");

/***/ }),

/***/ "./components/home/home-main.css":
/*!***************************************!*\
  !*** ./components/home/home-main.css ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./components/home/home-main.css?");

/***/ }),

/***/ "./components/home/home-red.css":
/*!**************************************!*\
  !*** ./components/home/home-red.css ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./components/home/home-red.css?");

/***/ }),

/***/ "./components/home/home-videos.css":
/*!*****************************************!*\
  !*** ./components/home/home-videos.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./components/home/home-videos.css?");

/***/ }),

/***/ "./components/plugins/ads.css":
/*!************************************!*\
  !*** ./components/plugins/ads.css ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./components/plugins/ads.css?");

/***/ }),

/***/ "./components/plugins/den.js":
/*!***********************************!*\
  !*** ./components/plugins/den.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\nexports.default = void 0;\n\nvar DreamsEngine = function DreamsEngine() {\n  var cssRule =\n  /* eslint-disable no-useless-concat */\n  'color: rgb(89, 178, 204);' + 'font-size: 10px;' + 'font-weight: bold;';\n  var den = '%c===================================================================\\n' + '== happy & brilliant people at... =================================\\n' + '===================================================================\\n' + ' ____                               _____             _\\n' + '|  _ \\\\ _ __ ___  __ _ _ __ ___  ___| ____|_ __   __ _(_)_ __   ___\\n' + \"| | | | '__/ _ \\\\/ _` | '_ ` _ \\\\/ __|  _| | '_ \\\\ / _` | | '_ \\\\ / _ \\\\\\n\" + '| |_| | | |  __/ (_| | | | | | \\\\__ \\\\ |___| | | | (_| | | | | |  __/\\n' + '|____/|_|  \\\\___|\\\\__,_|_| |_| |_|___/_____|_| |_|\\\\__, |_|_| |_|\\\\___|\\n' + '                                                |___/\\n' + '===================================================================\\n' + '=== http://dreamsengine.io ========================================\\n' + '===================================================================';\n  /* eslint-disable no-console */\n\n  console.info(den, cssRule);\n};\n\nvar _default = DreamsEngine;\nexports.default = _default;\n\n//# sourceURL=webpack:///./components/plugins/den.js?");

/***/ }),

/***/ "./components/sidebar/sidebar.css":
/*!****************************************!*\
  !*** ./components/sidebar/sidebar.css ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./components/sidebar/sidebar.css?");

/***/ }),

/***/ "./components/singles/ISAds.js":
/*!*************************************!*\
  !*** ./components/singles/ISAds.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\nexports.default = void 0;\n\n/* eslint-disable no-console */\nvar createAds = function createAds() {\n  var firstLot = function firstLot() {\n    var firstlots = document.querySelectorAll(\"#pinno-content-main\");\n    firstlots.forEach(function (flot) {\n      console.info(\"Creating Ads for Single View:\\n\");\n      console.info(\"For flot: \".concat(flot));\n      var dataFirstArticle = flot.dataset.postId;\n      var newFirstAdSlot = document.createElement(\"div\");\n      newFirstAdSlot.className = \"pinno-ad-serving pinno-ad-center\";\n      newFirstAdSlot.setAttribute(\"data-post-id\", dataFirstArticle);\n      var newFirstAd = document.createElement(\"div\");\n      newFirstAd.id = \"div-gpt-ad-mm-default-inread-\".concat(dataFirstArticle);\n      var adFirstWrapper = document.createElement(\"div\");\n      adFirstWrapper.id = \"pinno-post-bot-ad\";\n      adFirstWrapper.className = \"left relative\";\n      var adFirstTitles = document.createElement(\"span\");\n      adFirstTitles.className = \"pinno-ad-label\";\n      adFirstTitles.innerHTML = \"PUBLICIDAD\";\n      adFirstWrapper.appendChild(adFirstTitles);\n      newFirstAdSlot.appendChild(adFirstWrapper);\n      newFirstAdSlot.appendChild(newFirstAd);\n      var firstChild = flot.children[1];\n      flot.insertBefore(newFirstAdSlot, firstChild);\n    });\n  };\n\n  var secondLot = function secondLot() {\n    var secondLots = document.querySelectorAll(\".pinno-post-add-main\");\n    secondLots.forEach(function (seLot) {\n      console.log(\"For seLot: \".concat(seLot));\n      var dataSecondArticle = seLot.dataset.postId;\n      var newSeAdSlot = document.createElement(\"div\");\n      newSeAdSlot.className = \"pinno-ad-serving pinno-ad-center\";\n      newSeAdSlot.setAttribute(\"data-post-id\", dataSecondArticle);\n      var newSecondAd = document.createElement(\"div\");\n      newSecondAd.id = \"div-gpt-ad-mm-default-inread-\".concat(dataSecondArticle);\n      var adSecondWrapper = document.createElement(\"div\");\n      adSecondWrapper.id = \"pinno-post-bot-ad\";\n      adSecondWrapper.className = \"left relative\";\n      var adSecondTitles = document.createElement(\"span\");\n      adSecondTitles.className = \"pinno-ad-label\";\n      adSecondTitles.innerHTML = \"PUBLICIDAD\";\n      adSecondWrapper.appendChild(adSecondTitles);\n      newSeAdSlot.appendChild(adSecondWrapper);\n      newSeAdSlot.appendChild(newSecondAd);\n      var secondChild = seLot.children[1];\n      seLot.insertBefore(newSeAdSlot, secondChild);\n    });\n  };\n\n  firstLot();\n  secondLot();\n  console.info(\"Ads Creted\");\n};\n\nvar _default = createAds;\nexports.default = _default;\n\n//# sourceURL=webpack:///./components/singles/ISAds.js?");

/***/ }),

/***/ "./variables sync recursive \\.css$":
/*!*******************************!*\
  !*** ./variables sync \.css$ ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var map = {\n\t\"./variables.css\": \"./variables/variables.css\"\n};\n\n\nfunction webpackContext(req) {\n\tvar id = webpackContextResolve(req);\n\treturn __webpack_require__(id);\n}\nfunction webpackContextResolve(req) {\n\tif(!__webpack_require__.o(map, req)) {\n\t\tvar e = new Error(\"Cannot find module '\" + req + \"'\");\n\t\te.code = 'MODULE_NOT_FOUND';\n\t\tthrow e;\n\t}\n\treturn map[req];\n}\nwebpackContext.keys = function webpackContextKeys() {\n\treturn Object.keys(map);\n};\nwebpackContext.resolve = webpackContextResolve;\nmodule.exports = webpackContext;\nwebpackContext.id = \"./variables sync recursive \\\\.css$\";\n\n//# sourceURL=webpack:///./variables_sync_\\.css$?");

/***/ }),

/***/ "./variables/variables.css":
/*!*********************************!*\
  !*** ./variables/variables.css ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./variables/variables.css?");

/***/ }),

/***/ 0:
/*!**********************!*\
  !*** multi ./app.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = __webpack_require__(/*! ./app.js */\"./app.js\");\n\n\n//# sourceURL=webpack:///multi_./app.js?");

/***/ })

/******/ });