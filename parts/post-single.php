<?php get_header(); ?>
<?php global $author; $userdata = get_userdata($author); ?>
<article id="pinno-article-wrap" itemscope itemtype="http://schema.org/NewsArticle" class="pedro">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<meta itemscope itemprop="mainEntityOfPage"  itemType="https://schema.org/WebPage" itemid="<?php the_permalink(); ?>"/>
		<?php global $post; $pinno_post_layout = get_option('pinno_post_layout'); $pinno_post_temp = get_post_meta($post->ID, "pinno_post_template", true); if( ( empty($pinno_post_temp) && $pinno_post_layout == '6' ) || ( $pinno_post_temp == "def" && $pinno_post_layout == '6' ) || ( empty($pinno_post_temp) && $pinno_post_layout == '7' ) || ( $pinno_post_temp == "def" && $pinno_post_layout == '7' ) || ( $pinno_post_temp == "global" && $pinno_post_layout == '6' ) || ( $pinno_post_temp == "global" && $pinno_post_layout == '7' ) || $pinno_post_temp == "temp7" || $pinno_post_temp == "temp8" ) { ?>
			<div id="pinno-vid-wide-wrap" class="left relative">
				<div class="pinno-main-box">
					<div class="pinno-vid-wide-cont left relative">
						<div class="pinno-vid-wide-top left relative">
							<div class="pinno-vid-wide-out left relative">
								<div class="pinno-vid-wide-in">
									<div class="pinno-vid-wide-left left relative">
										<?php if(get_post_meta($post->ID, "pinno_video_embed", true)) { ?>
											<div id="pinno-video-embed-wrap" class="left relative">
												<div id="pinno-video-embed-cont" class="left relative">
													<span class="pinno-video-close fa fa-times" aria-hidden="true"></span>
													<div id="pinno-video-embed" class="left relative">
														<?php echo html_entity_decode(get_post_meta($post->ID, "pinno_video_embed", true)); ?>
													</div><!--pinno-video-embed-->
												</div><!--pinno-video-embed-cont-->
											</div><!--pinno-video-embed-wrap-->
											<div class="pinno-post-img-hide" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
												<?php $thumb_id = get_post_thumbnail_id(); $pinno_thumb_array = wp_get_attachment_image_src($thumb_id, 'pinno-post-thumb', true); $pinno_thumb_url = $pinno_thumb_array[0]; $pinno_thumb_width = $pinno_thumb_array[1]; $pinno_thumb_height = $pinno_thumb_array[2]; ?>
												<meta itemprop="url" content="<?php echo esc_url($pinno_thumb_url) ?>">
												<meta itemprop="width" content="<?php echo esc_html($pinno_thumb_width) ?>">
												<meta itemprop="height" content="<?php echo esc_html($pinno_thumb_height) ?>">
											</div><!--pinno-post-img-hide-->
										<?php } else { ?>
											<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
												<div id="pinno-post-feat-img" class="left relative pinno-post-feat-img-wide2" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
													<?php the_post_thumbnail(''); ?>
													<?php $thumb_id = get_post_thumbnail_id(); $pinno_thumb_array = wp_get_attachment_image_src($thumb_id, 'pinno-post-thumb', true); $pinno_thumb_url = $pinno_thumb_array[0]; $pinno_thumb_width = $pinno_thumb_array[1]; $pinno_thumb_height = $pinno_thumb_array[2]; ?>
													<meta itemprop="url" content="<?php echo esc_url($pinno_thumb_url) ?>">
													<meta itemprop="width" content="<?php echo esc_html($pinno_thumb_width) ?>">
													<meta itemprop="height" content="<?php echo esc_html($pinno_thumb_height) ?>">
												</div><!--pinno-post-feat-img-->
											<?php } ?>
										<?php } ?>
									</div><!--pinno-vid-wide-left-->
								</div><!--pinno-vid-wide-in-->
								<div class="pinno-vid-wide-right left relative">
									<div class="pinno-vid-wide-text left relative">
										<div class="pinno-cat-date-wrap left relative">
											<a class="pinno-post-cat-link" href="<?php $category = get_the_category(); $category_id = get_cat_ID( $category[0]->cat_name ); $category_link = get_category_link( $category_id ); echo esc_url( $category_link ); ?>"><span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><a>
										</div><!--pinno-cat-date-wrap-->
										<h1 class="pinno-post-title pinno-vid-wide-title left entry-title" itemprop="headline"><?php the_title(); ?></h1>
										<?php if ( has_excerpt() ) { ?>
											<span class="pinno-post-excerpt left"><?php the_excerpt(); ?></span>
										<?php } ?>
									</div><!--pinno-vid-wide-text-->
									<?php $socialbox = get_option('pinno_social_box'); if ($socialbox == "true") { ?>
										<?php if ( function_exists( 'pinno_SocialSharingVid' ) ) { ?>
											<?php pinno_SocialSharingVid(); ?>
										<?php } ?>
									<?php } ?>
								</div><!--pinno-vid-wide-right-->
							</div><!--pinno-vid-wide-out-->
						</div><!--pinno-vid-wide-top-->
						<div class="pinno-vid-wide-bot left relative">
							<h4 class="pinno-widget-home-title">
								<span class="pinno-widget-home-title"><?php esc_html_e( "More Videos", 'iggy-type-0' ); ?></span>
							</h4>
							<div class="pinno-vid-wide-more-wrap left relative">
								<ul class="pinno-vid-wide-more-list left relative">
									<?php global $post; query_posts(array( 'tax_query' => array( array( 'taxonomy' => 'post_format', 'field' => 'slug', 'terms' => 'post-format-video' )), 'posts_per_page' => '4', 'post__not_in' => array($post->ID) )); if (have_posts()) : while (have_posts()) : the_post(); ?>
										<a href="<?php the_permalink(); ?>" rel="bookmark">
										<li>
											<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
												<div class="pinno-vid-wide-more-img left relative">
													<?php the_post_thumbnail('pinno-mid-thumb', array( 'class' => 'pinno-reg-img' )); ?>
													<?php the_post_thumbnail('pinno-small-thumb', array( 'class' => 'pinno-mob-img' )); ?>
													<?php if ( has_post_format( 'video' )) { ?>
														<div class="pinno-vid-box-wrap pinno-vid-marg">
															<i class="fa fa-2 fa-play" aria-hidden="true"></i>
														</div><!--pinno-vid-box-wrap-->
													<?php } ?>
												</div><!--pinno-vid-wide-more-img-->
											<?php } ?>
											<div class="pinno-vid-wide-more-text left relative">
												<p><?php the_title(); ?></p>
											</div><!--pinno-vid-wide-more-text-->
										</li>
										</a>
									<?php endwhile; endif; wp_reset_query(); ?>
								</ul>
							</div><!--pinno-vid-wide-more-wrap-->
						</div><!--pinno-vid-wide-bot-->
					</div><!--pinno-vid-wide-cont-->
				</div><!--pinno-main-box-->
			</div><!--pinno-vid-wide-wrap-->
		<?php } ?>
		<?php global $post; $pinno_post_layout = get_option('pinno_post_layout'); $pinno_post_temp = get_post_meta($post->ID, "pinno_post_template", true); if( ( empty($pinno_post_temp) && $pinno_post_layout == '4' ) || ( $pinno_post_temp == "def" && $pinno_post_layout == '4' ) || ( empty($pinno_post_temp) && $pinno_post_layout == '5' ) || ( $pinno_post_temp == "def" && $pinno_post_layout == '5' ) || ( $pinno_post_temp == "global" && $pinno_post_layout == '4' ) || ( $pinno_post_temp == "global" && $pinno_post_layout == '5' ) || $pinno_post_temp == "temp5" || $pinno_post_temp == "temp6" ) { ?>
			<?php $pinno_featured_img = get_option('pinno_featured_img'); $pinno_show_hide = get_post_meta($post->ID, "pinno_featured_image", true); if ($pinno_featured_img == "true") { if ($pinno_show_hide !== "hide") { ?>
				<?php if(get_post_meta($post->ID, "pinno_video_embed", true)) { ?>
					<div class="pinno-main-body-max">
						<div id="pinno-video-embed" class="left relative pinno-video-embed-wide">
							<?php echo html_entity_decode(get_post_meta($post->ID, "pinno_video_embed", true)); ?>
						</div><!--pinno-video-embed-->
					</div><!--pinno-main-body-max-->
				<?php } else { ?>
					<div class="pinno-main-body-max">
						<div id="pinno-post-feat-img-wide" class="left relative">
							<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
								<div id="pinno-post-feat-img" class="left relative pinno-post-feat-img-wide2" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
									<?php the_post_thumbnail('', array( 'class' => 'pinno-reg-img' )); ?>
									<?php the_post_thumbnail('pinno-port-thumb', array( 'class' => 'pinno-mob-img' )); ?>
									<?php $thumb_id = get_post_thumbnail_id(); $pinno_thumb_array = wp_get_attachment_image_src($thumb_id, 'pinno-post-thumb', true); $pinno_thumb_url = $pinno_thumb_array[0]; $pinno_thumb_width = $pinno_thumb_array[1]; $pinno_thumb_height = $pinno_thumb_array[2]; ?>
									<meta itemprop="url" content="<?php echo esc_url($pinno_thumb_url) ?>">
									<meta itemprop="width" content="<?php echo esc_html($pinno_thumb_width) ?>">
									<meta itemprop="height" content="<?php echo esc_html($pinno_thumb_height) ?>">
								</div><!--pinno-post-feat-img-->
							<?php } ?>
							<div id="pinno-post-feat-text-wrap" class="left relative">
								<div class="pinno-post-feat-text-main">
									<div class="pinno-post-feat-text left relative">
										<h3 class="pinno-post-cat left relative"><a class="pinno-post-cat-link" href="<?php $category = get_the_category(); $category_id = get_cat_ID( $category[0]->cat_name ); $category_link = get_category_link( $category_id ); echo esc_url( $category_link ); ?>"><span class="pinno-post-cat left"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span></a></h3>
										<h1 class="pinno-post-title pinno-post-title-wide left entry-title" itemprop="headline"><?php the_title(); ?></h1>
										<?php if ( has_excerpt() ) { ?>
											<span class="pinno-post-excerpt left"><?php the_excerpt(); ?></span>
										<?php } ?>
									</div><!--pinno-post-feat-text-->
								</div><!--pinno-post-feat-text-main-->
							</div><!--pinno-post-feat-text-wrap-->
							<?php global $post; if(get_post_meta($post->ID, "pinno_photo_credit", true)): ?>
								<span class="pinno-feat-caption"><?php echo wp_kses_post(get_post_meta($post->ID, "pinno_photo_credit", true)); ?></span>
							<?php endif; ?>
						</div><!--pinno-post-feat-img-wide-->
					</div><!--pinno-main-body-max-->
				<?php } ?>
			<?php } else { ?>
				<div class="pinno-post-img-hide" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
					<?php $thumb_id = get_post_thumbnail_id(); $pinno_thumb_array = wp_get_attachment_image_src($thumb_id, 'pinno-post-thumb', true); $pinno_thumb_url = $pinno_thumb_array[0]; $pinno_thumb_width = $pinno_thumb_array[1]; $pinno_thumb_height = $pinno_thumb_array[2]; ?>
					<meta itemprop="url" content="<?php echo esc_url($pinno_thumb_url) ?>">
					<meta itemprop="width" content="<?php echo esc_html($pinno_thumb_width) ?>">
					<meta itemprop="height" content="<?php echo esc_html($pinno_thumb_height) ?>">
				</div><!--pinno-post-img-hide-->
			<?php } ?>
			<?php } else { ?>
				<div class="pinno-post-img-hide" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
					<?php $thumb_id = get_post_thumbnail_id(); $pinno_thumb_array = wp_get_attachment_image_src($thumb_id, 'pinno-post-thumb', true); $pinno_thumb_url = $pinno_thumb_array[0]; $pinno_thumb_width = $pinno_thumb_array[1]; $pinno_thumb_height = $pinno_thumb_array[2]; ?>
					<meta itemprop="url" content="<?php echo esc_url($pinno_thumb_url) ?>">
					<meta itemprop="width" content="<?php echo esc_html($pinno_thumb_width) ?>">
					<meta itemprop="height" content="<?php echo esc_html($pinno_thumb_height) ?>">
				</div><!--pinno-post-img-hide-->
			<?php } ?>
		<?php } ?>
		<div id="pinno-article-cont" class="left relative">
			<div class="pinno-main-box">
				<div id="pinno-post-main" class="left relative">
					<?php global $post; $pinno_post_layout = get_option('pinno_post_layout'); $pinno_post_temp = get_post_meta($post->ID, "pinno_post_template", true); if( ( empty($pinno_post_temp) && empty($pinno_post_layout) ) || ( empty($pinno_post_temp) && $pinno_post_layout == '0' ) || ( $pinno_post_temp == "def" && $pinno_post_layout == '0' ) || ( empty($pinno_post_temp) && $pinno_post_layout == '1' ) || ( $pinno_post_temp == "def" && $pinno_post_layout == "1" ) || ( $pinno_post_temp == "global" && $pinno_post_layout == '0' ) || ( $pinno_post_temp == "global" && $pinno_post_layout == '1' ) || $pinno_post_temp == "temp1" || $pinno_post_temp == "temp2" ) { ?>
					<header id="pinno-post-head" class="left relative">
						<h3 class="pinno-post-cat left relative"><a class="pinno-post-cat-link" href="<?php $category = get_the_category(); $category_id = get_cat_ID( $category[0]->cat_name ); $category_link = get_category_link( $category_id ); echo esc_url( $category_link ); ?>"><span class="pinno-post-cat left"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span></a></h3>
						<h1 class="pinno-post-title left entry-title" itemprop="headline"><?php the_title(); ?></h1>
						<?php if ( has_excerpt() ) { ?>
							<span class="pinno-post-excerpt left"><?php the_excerpt(); ?></span>
						<?php } ?>
						<?php $author_info = get_option('pinno_author_info'); if ($author_info == "true") { ?>
							<div class="pinno-author-info-wrap left relative">
								<div class="pinno-author-info-thumb left relative">
									<?php echo get_avatar( get_the_author_meta('email'), '46' ); ?>
								</div><!--pinno-author-info-thumb-->
								<div class="pinno-author-info-text left relative">
									<div class="pinno-author-info-date left relative">
										<p><?php esc_html_e( 'Published', 'iggy-type-0' ); ?></p> <span class="pinno-post-date"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span> <p><?php esc_html_e( 'on', 'iggy-type-0' ); ?></p> <span class="pinno-post-date updated"><time class="post-date updated" itemprop="datePublished" datetime="<?php the_time('Y-m-d'); ?>"><?php the_time(get_option('date_format')); ?></time></span>
										<meta itemprop="dateModified" content="<?php the_modified_date('Y-m-d g:i a'); ?>"/>
									</div><!--pinno-author-info-date-->
									<div class="pinno-author-info-name left relative" itemprop="author" itemscope itemtype="https://schema.org/Person">
										<p><?php esc_html_e( 'By', 'iggy-type-0' ); ?></p> <span class="author-name vcard fn author" itemprop="name"><?php the_author_posts_link(); ?></span> <?php $authortwit = get_the_author_meta('twitter'); if ( ! empty ( $authortwit ) ) { ?><a href="<?php echo esc_url(the_author_meta('twitter')); ?>" class="pinno-twit-but" target="_blank"><span class="pinno-author-info-twit-but"><i class="fa fa-twitter fa-2"></i></span></a><?php } ?>
									</div><!--pinno-author-info-name-->
								</div><!--pinno-author-info-text-->
							</div><!--pinno-author-info-wrap-->
						<?php } ?>
					</header>
					<?php } ?>
					<div class="pinno-post-main-out left relative">
						<div class="pinno-post-main-in">
							<div id="pinno-post-content" class="left relative">
								<?php global $post; $pinno_post_layout = get_option('pinno_post_layout'); $pinno_post_temp = get_post_meta($post->ID, "pinno_post_template", true); if( ( empty($pinno_post_temp) && $pinno_post_layout == '2' ) || ( $pinno_post_temp == "def" && $pinno_post_layout == '2' ) || ( empty($pinno_post_temp) && $pinno_post_layout == '3' ) || ( $pinno_post_temp == "def" && $pinno_post_layout == '3' ) || ( $pinno_post_temp == "global" && $pinno_post_layout == '2' ) || ( $pinno_post_temp == "global" && $pinno_post_layout == '3' ) || $pinno_post_temp == "temp3" || $pinno_post_temp == "temp4" ) { ?>
									<header id="pinno-post-head" class="left relative">
										<h3 class="pinno-post-cat left relative"><a class="pinno-post-cat-link" href="<?php $category = get_the_category(); $category_id = get_cat_ID( $category[0]->cat_name ); $category_link = get_category_link( $category_id ); echo esc_url( $category_link ); ?>"><span class="pinno-post-cat left"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span></a></h3>
										<h1 class="pinno-post-title left entry-title" itemprop="headline"><?php the_title(); ?></h1>
										<?php if ( has_excerpt() ) { ?>
											<span class="pinno-post-excerpt left"><?php the_excerpt(); ?></span>
										<?php } ?>
										<?php $author_info = get_option('pinno_author_info'); if ($author_info == "true") { ?>
											<div class="pinno-author-info-wrap left relative">
												<div class="pinno-author-info-thumb left relative">
													<?php echo get_avatar( get_the_author_meta('email'), '46' ); ?>
												</div><!--pinno-author-info-thumb-->
												<div class="pinno-author-info-text left relative">
													<div class="pinno-author-info-date left relative">
														<p><?php esc_html_e( 'Published', 'iggy-type-0' ); ?></p> <span class="pinno-post-date"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span> <p><?php esc_html_e( 'on', 'iggy-type-0' ); ?></p> <span class="pinno-post-date updated"><time class="post-date updated" itemprop="datePublished" datetime="<?php the_time('Y-m-d'); ?>"><?php the_time(get_option('date_format')); ?></time></span>
														<meta itemprop="dateModified" content="<?php the_modified_date('Y-m-d'); ?>"/>
													</div><!--pinno-author-info-date-->
													<div class="pinno-author-info-name left relative" itemprop="author" itemscope itemtype="https://schema.org/Person">
														<p><?php esc_html_e( 'By', 'iggy-type-0' ); ?></p> <span class="author-name vcard fn author" itemprop="name"><?php the_author_posts_link(); ?></span> <?php $authortwit = get_the_author_meta('twitter'); if ( ! empty ( $authortwit ) ) { ?><a href="<?php echo esc_url(the_author_meta('twitter')); ?>" class="pinno-twit-but" target="_blank"><span class="pinno-author-info-twit-but"><i class="fa fa-twitter fa-2"></i></span></a><?php } ?>
													</div><!--pinno-author-info-name-->
												</div><!--pinno-author-info-text-->
											</div><!--pinno-author-info-wrap-->
										<?php } ?>
									</header>
								<?php } ?>
								<?php global $post; $pinno_post_layout = get_option('pinno_post_layout'); $pinno_post_temp = get_post_meta($post->ID, "pinno_post_template", true); if( ( empty($pinno_post_temp) && $pinno_post_layout == '4' ) || ( $pinno_post_temp == "def" && $pinno_post_layout == '4' ) || ( empty($pinno_post_temp) && $pinno_post_layout == '5' ) || ( $pinno_post_temp == "def" && $pinno_post_layout == '5' ) || ( $pinno_post_temp == "global" && $pinno_post_layout == '4' ) || ( $pinno_post_temp == "global" && $pinno_post_layout == '5' ) || $pinno_post_temp == "temp5" || $pinno_post_temp == "temp6" ) { ?>
									<?php if(get_post_meta($post->ID, "pinno_video_embed", true)) { ?>
									<header id="pinno-post-head" class="left relative">
										<h3 class="pinno-post-cat left relative"><a class="pinno-post-cat-link" href="<?php $category = get_the_category(); $category_id = get_cat_ID( $category[0]->cat_name ); $category_link = get_category_link( $category_id ); echo esc_url( $category_link ); ?>"><span class="pinno-post-cat left"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span></a></h3>
										<h1 class="pinno-post-title left entry-title" itemprop="headline"><?php the_title(); ?></h1>
										<?php if ( has_excerpt() ) { ?>
											<span class="pinno-post-excerpt left"><?php the_excerpt(); ?></span>
										<?php } ?>
										<?php $author_info = get_option('pinno_author_info'); if ($author_info == "true") { ?>
											<div class="pinno-author-info-wrap left relative">
												<div class="pinno-author-info-thumb left relative">
													<?php echo get_avatar( get_the_author_meta('email'), '46' ); ?>
												</div><!--pinno-author-info-thumb-->
												<div class="pinno-author-info-text left relative">
													<div class="pinno-author-info-date left relative">
														<p><?php esc_html_e( 'Published', 'iggy-type-0' ); ?></p> <span class="pinno-post-date"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span> <p><?php esc_html_e( 'on', 'iggy-type-0' ); ?></p> <span class="pinno-post-date updated"><time class="post-date updated" itemprop="datePublished" datetime="<?php the_time('Y-m-d'); ?>"><?php the_time(get_option('date_format')); ?></time></span>
														<meta itemprop="dateModified" content="<?php the_modified_date('Y-m-d'); ?>"/>
													</div><!--pinno-author-info-date-->
													<div class="pinno-author-info-name left relative" itemprop="author" itemscope itemtype="https://schema.org/Person">
														<p><?php esc_html_e( 'By', 'iggy-type-0' ); ?></p> <span class="author-name vcard fn author" itemprop="name"><?php the_author_posts_link(); ?></span> <?php $authortwit = get_the_author_meta('twitter'); if ( ! empty ( $authortwit ) ) { ?><a href="<?php echo esc_url(the_author_meta('twitter')); ?>" class="pinno-twit-but" target="_blank"><span class="pinno-author-info-twit-but"><i class="fa fa-twitter fa-2"></i></span></a><?php } ?>
													</div><!--pinno-author-info-name-->
												</div><!--pinno-author-info-text-->
											</div><!--pinno-author-info-wrap-->
										<?php } ?>
									</header>
									<?php } ?>
								<?php } ?>
								<?php global $post; $pinno_post_layout = get_option('pinno_post_layout'); $pinno_post_temp = get_post_meta($post->ID, "pinno_post_template", true); if( ( empty($pinno_post_temp) && empty($pinno_post_layout) ) || ( empty($pinno_post_temp) && $pinno_post_layout == '0' ) || ( $pinno_post_temp == "def" && $pinno_post_layout == '0' ) || ( empty($pinno_post_temp) && $pinno_post_layout == '1' ) || ( $pinno_post_temp == "def" && $pinno_post_layout == '1' ) || ( empty($pinno_post_temp) && $pinno_post_layout == '2' ) || ( $pinno_post_temp == "def" && $pinno_post_layout == '2' ) || ( empty($pinno_post_temp) && $pinno_post_layout == '3' ) || ( $pinno_post_temp == "def" && $pinno_post_layout == '3' ) || ( $pinno_post_temp == "global" && $pinno_post_layout == '0' ) || ( $pinno_post_temp == "global" && $pinno_post_layout == '1' ) || ( $pinno_post_temp == "global" && $pinno_post_layout == '2' ) || ( $pinno_post_temp == "global" && $pinno_post_layout == '3' ) || $pinno_post_temp == "temp1" || $pinno_post_temp == "temp2" || $pinno_post_temp == "temp3" || $pinno_post_temp == "temp4" ) { ?>
								<?php $pinno_featured_img = get_option('pinno_featured_img'); $pinno_show_hide = get_post_meta($post->ID, "pinno_featured_image", true); if ($pinno_featured_img == "true") { if ($pinno_show_hide !== "hide") { ?>
									<?php if(get_post_meta($post->ID, "pinno_video_embed", true)) { ?>
										<div id="pinno-video-embed-wrap" class="left relative">
											<div id="pinno-video-embed-cont" class="left relative">
												<span class="pinno-video-close fa fa-times" aria-hidden="true"></span>
												<div id="pinno-video-embed" class="left relative">
													<?php echo html_entity_decode(get_post_meta($post->ID, "pinno_video_embed", true)); ?>
												</div><!--pinno-video-embed-->
											</div><!--pinno-video-embed-cont-->
										</div><!--pinno-video-embed-wrap-->
										<div class="pinno-post-img-hide" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
											<?php $thumb_id = get_post_thumbnail_id(); $pinno_thumb_array = wp_get_attachment_image_src($thumb_id, 'pinno-post-thumb', true); $pinno_thumb_url = $pinno_thumb_array[0]; $pinno_thumb_width = $pinno_thumb_array[1]; $pinno_thumb_height = $pinno_thumb_array[2]; ?>
											<meta itemprop="url" content="<?php echo esc_url($pinno_thumb_url) ?>">
											<meta itemprop="width" content="<?php echo esc_html($pinno_thumb_width) ?>">
											<meta itemprop="height" content="<?php echo esc_html($pinno_thumb_height) ?>">
										</div><!--pinno-post-img-hide-->
									<?php } else { ?>
										<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
											<div id="pinno-post-feat-img" class="left relative pinno-post-feat-img-wide2" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
												<?php the_post_thumbnail(''); ?>
												<?php $thumb_id = get_post_thumbnail_id(); $pinno_thumb_array = wp_get_attachment_image_src($thumb_id, 'pinno-post-thumb', true); $pinno_thumb_url = $pinno_thumb_array[0]; $pinno_thumb_width = $pinno_thumb_array[1]; $pinno_thumb_height = $pinno_thumb_array[2]; ?>
												<meta itemprop="url" content="<?php echo esc_url($pinno_thumb_url) ?>">
												<meta itemprop="width" content="<?php echo esc_html($pinno_thumb_width) ?>">
												<meta itemprop="height" content="<?php echo esc_html($pinno_thumb_height) ?>">
											</div><!--pinno-post-feat-img-->
											<?php global $post; if(get_post_meta($post->ID, "pinno_photo_credit", true)): ?>
												<span class="pinno-feat-caption"><?php echo wp_kses_post(get_post_meta($post->ID, "pinno_photo_credit", true)); ?></span>
											<?php endif; ?>
										<?php } ?>
									<?php } ?>
								<?php } else { ?>
									<div class="pinno-post-img-hide" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
										<?php $thumb_id = get_post_thumbnail_id(); $pinno_thumb_array = wp_get_attachment_image_src($thumb_id, 'pinno-post-thumb', true); $pinno_thumb_url = $pinno_thumb_array[0]; $pinno_thumb_width = $pinno_thumb_array[1]; $pinno_thumb_height = $pinno_thumb_array[2]; ?>
										<meta itemprop="url" content="<?php echo esc_url($pinno_thumb_url) ?>">
										<meta itemprop="width" content="<?php echo esc_html($pinno_thumb_width) ?>">
										<meta itemprop="height" content="<?php echo esc_html($pinno_thumb_height) ?>">
									</div><!--pinno-post-img-hide-->
								<?php } ?>
								<?php } else { ?>
									<div class="pinno-post-img-hide" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
										<?php $thumb_id = get_post_thumbnail_id(); $pinno_thumb_array = wp_get_attachment_image_src($thumb_id, 'pinno-post-thumb', true); $pinno_thumb_url = $pinno_thumb_array[0]; $pinno_thumb_width = $pinno_thumb_array[1]; $pinno_thumb_height = $pinno_thumb_array[2]; ?>
										<meta itemprop="url" content="<?php echo esc_url($pinno_thumb_url) ?>">
										<meta itemprop="width" content="<?php echo esc_html($pinno_thumb_width) ?>">
										<meta itemprop="height" content="<?php echo esc_html($pinno_thumb_height) ?>">
									</div><!--pinno-post-img-hide-->
								<?php } ?>
								<?php } ?>
								<div id="pinno-content-wrap" class="left relative">
									<div class="pinno-post-soc-out right relative">
										<?php $socialbox = get_option('pinno_social_box'); if ($socialbox == "true") { ?>
											<?php if ( function_exists( 'pinno_SocialSharing' ) ) { ?>
												<?php pinno_SocialSharing(); ?>
											<?php } ?>
										<?php } ?>
										<div class="pinno-post-soc-in">
											<div id="pinno-content-body" class="left relative">
												<div id="pinno-content-body-top" class="left relative">
													<?php global $post; $pinno_post_layout = get_option('pinno_post_layout'); $pinno_post_temp = get_post_meta($post->ID, "pinno_post_template", true); if( ( empty($pinno_post_temp) && $pinno_post_layout == '4' ) || ( $pinno_post_temp == "def" && $pinno_post_layout == '4' ) || ( empty($pinno_post_temp) && $pinno_post_layout == '5' ) || ( $pinno_post_temp == "def" && $pinno_post_layout == '5' ) || ( $pinno_post_temp == "global" && $pinno_post_layout == '4' ) || ( $pinno_post_temp == "global" && $pinno_post_layout == '5' ) || $pinno_post_temp == "temp5" || $pinno_post_temp == "temp6" ) { ?>
														<?php if(get_post_meta($post->ID, "pinno_video_embed", true)) { } else { ?>
															<?php $author_info = get_option('pinno_author_info'); if ($author_info == "true") { ?>
																<div class="pinno-author-info-wrap left relative">
																	<div class="pinno-author-info-thumb left relative">
																		<?php echo get_avatar( get_the_author_meta('email'), '46' ); ?>
																	</div><!--pinno-author-info-thumb-->
																	<div class="pinno-author-info-text left relative">
																		<div class="pinno-author-info-date left relative">
																			<p><?php esc_html_e( 'Published', 'iggy-type-0' ); ?></p> <span class="pinno-post-date"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span> <p><?php esc_html_e( 'on', 'iggy-type-0' ); ?></p> <span class="pinno-post-date updated"><time class="post-date updated" itemprop="datePublished" datetime="<?php the_time('Y-m-d'); ?>"><?php the_time(get_option('date_format')); ?></time></span>
																			<meta itemprop="dateModified" content="<?php the_modified_date('Y-m-d'); ?>"/>
																		</div><!--pinno-author-info-date-->
																		<div class="pinno-author-info-name left relative" itemprop="author" itemscope itemtype="https://schema.org/Person">
																			<p><?php esc_html_e( 'By', 'iggy-type-0' ); ?></p> <span class="author-name vcard fn author" itemprop="name"><?php the_author_posts_link(); ?></span> <?php $authortwit = get_the_author_meta('twitter'); if ( ! empty ( $authortwit ) ) { ?><a href="<?php echo esc_url(the_author_meta('twitter')); ?>" class="pinno-twit-but" target="_blank"><span class="pinno-author-info-twit-but"><i class="fa fa-twitter fa-2"></i></span></a><?php } ?>
																		</div><!--pinno-author-info-name-->
																	</div><!--pinno-author-info-text-->
																</div><!--pinno-author-info-wrap-->
															<?php } ?>
														<?php } ?>
													<?php } ?>
													<?php global $post; $pinno_post_layout = get_option('pinno_post_layout'); $pinno_post_temp = get_post_meta($post->ID, "pinno_post_template", true); if( ( empty($pinno_post_temp) && $pinno_post_layout == '6' ) || ( $pinno_post_temp == "def" && $pinno_post_layout == '6' ) || ( empty($pinno_post_temp) && $pinno_post_layout == '7' ) || ( $pinno_post_temp == "def" && $pinno_post_layout == '7' ) || ( $pinno_post_temp == "global" && $pinno_post_layout == '6' ) || ( $pinno_post_temp == "global" && $pinno_post_layout == '7' ) || $pinno_post_temp == "temp7" || $pinno_post_temp == "temp8" ) { ?>
														<?php $author_info = get_option('pinno_author_info'); if ($author_info == "true") { ?>
															<div class="pinno-author-info-wrap left relative">
																<div class="pinno-author-info-thumb left relative">
																	<?php echo get_avatar( get_the_author_meta('email'), '46' ); ?>
																</div><!--pinno-author-info-thumb-->
																<div class="pinno-author-info-text left relative">
																	<div class="pinno-author-info-date left relative">
																		<p><?php esc_html_e( 'Published', 'iggy-type-0' ); ?></p> <span class="pinno-post-date"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span> <p><?php esc_html_e( 'on', 'iggy-type-0' ); ?></p> <span class="pinno-post-date updated"><time class="post-date updated" itemprop="datePublished" datetime="<?php the_time('Y-m-d'); ?>"><?php the_time(get_option('date_format')); ?></time></span>
																		<meta itemprop="dateModified" content="<?php the_modified_date('Y-m-d'); ?>"/>
																	</div><!--pinno-author-info-date-->
																	<div class="pinno-author-info-name left relative" itemprop="author" itemscope itemtype="https://schema.org/Person">
																		<p><?php esc_html_e( 'By', 'iggy-type-0' ); ?></p> <span class="author-name vcard fn author" itemprop="name"><?php the_author_posts_link(); ?></span> <?php $authortwit = get_the_author_meta('twitter'); if ( ! empty ( $authortwit ) ) { ?><a href="<?php echo esc_url(the_author_meta('twitter')); ?>" class="pinno-twit-but" target="_blank"><span class="pinno-author-info-twit-but"><i class="fa fa-twitter fa-2"></i></span></a><?php } ?>
																	</div><!--pinno-author-info-name-->
																</div><!--pinno-author-info-text-->
															</div><!--pinno-author-info-wrap-->
														<?php } ?>
													<?php } ?>
													<div id="pinno-content-main" data-post-id="<?php the_id(); ?>" class="left relative">
														<?php the_content(); ?>
														<?php wp_link_pages(); ?>

													</div><!--pinno-content-main-->
													<div id="pinno-content-bot" class="left">
														<?php $pinno_show_gallery = get_post_meta($post->ID, "pinno_post_gallery", true); if ($pinno_show_gallery == "show") { ?>
															<section class="pinno-post-gallery-wrap left relative">
																<div class="pinno-post-gallery-top left relative flexslider">
																	<ul class="pinno-post-gallery-top-list slides">
																		<?php $images = get_attached_media('image', $post->ID); foreach($images as $image) { ?>
																			<li>
    																			<?php echo wp_get_attachment_image($image->ID, 'pinno-post-thumb'); ?>
																				<div class="pinno-post-gallery-text">
																					<p><?php echo wp_get_attachment_caption($image->ID); ?></p>
																				</div>
																			</li>
																		<?php } ?>
																	</ul>
																</div><!--pinno-post-gallery-top-->
																<div class="pinno-post-gallery-bot left relative flexslider">
																	<ul class="pinno-post-gallery-bot-list slides">
																		<?php $images = get_attached_media('image', $post->ID); foreach($images as $image) { ?>
																			<li>
    																			<?php echo wp_get_attachment_image($image->ID, 'pinno-small-thumb'); ?>
																			</li>
																		<?php } ?>
																	</ul>
																</div><!--pinno-post-gallery-bot-->
															</section><!--pinno-post-gallery-wrap-->
														<?php } ?>
														<div class="pinno-post-tags">
															<span class="pinno-post-tags-header"><?php esc_html_e( 'Related Topics:', 'iggy-type-0' ); ?></span><span itemprop="keywords"><?php the_tags('','','') ?></span>
														</div><!--pinno-post-tags-->
														<div class="posts-nav-link">
															<?php posts_nav_link(); ?>
														</div><!--posts-nav-link-->
														<?php $pinno_prev_next = get_option('pinno_prev_next'); if ($pinno_prev_next == "true") { ?>
															<div id="pinno-prev-next-wrap" class="left relative">
																<?php $nextPost = get_next_post(TRUE, ''); if($nextPost) { $args = array( 'posts_per_page' => 1, 'include' => $nextPost->ID ); $nextPost = get_posts($args); foreach ($nextPost as $post) { setup_postdata($post); ?>
																	<div class="pinno-next-post-wrap right relative">
																		<a href="<?php the_permalink(); ?>" rel="bookmark">
																		<div class="pinno-prev-next-cont left relative">
																			<div class="pinno-next-cont-out left relative">
																				<div class="pinno-next-cont-in">
																					<div class="pinno-prev-next-text left relative">
																						<span class="pinno-prev-next-label left relative"><?php esc_html_e( "Up Next", 'iggy-type-0' ); ?></span>
																						<p><?php the_title(); ?></p>
																					</div><!--pinno-prev-next-text-->
																				</div><!--pinno-next-cont-in-->
																				<span class="pinno-next-arr fa fa-chevron-right right"></span>
																			</div><!--pinno-prev-next-out-->
																		</div><!--pinno-prev-next-cont-->
																		</a>
																	</div><!--pinno-next-post-wrap-->
																<?php wp_reset_postdata(); } } ?>
																<?php $prevPost = get_previous_post(TRUE, ''); if($prevPost) { $args = array( 'posts_per_page' => 1, 'include' => $prevPost->ID ); $prevPost = get_posts($args); foreach ($prevPost as $post) { setup_postdata($post); ?>
																	<div class="pinno-prev-post-wrap left relative">
																		<a href="<?php the_permalink(); ?>" rel="bookmark">
																		<div class="pinno-prev-next-cont left relative">
																			<div class="pinno-prev-cont-out right relative">
																				<span class="pinno-prev-arr fa fa-chevron-left left"></span>
																				<div class="pinno-prev-cont-in">
																					<div class="pinno-prev-next-text left relative">
																						<span class="pinno-prev-next-label left relative"><?php esc_html_e( "Don't Miss", 'iggy-type-0' ); ?></span>
																						<p><?php the_title(); ?></p>
																					</div><!--pinno-prev-next-text-->
																				</div><!--pinno-prev-cont-in-->
																			</div><!--pinno-prev-cont-out-->
																		</div><!--pinno-prev-next-cont-->
																		</a>
																	</div><!--pinno-prev-post-wrap-->
																<?php wp_reset_postdata(); } } ?>
															</div><!--pinno-prev-next-wrap-->
														<?php } ?>
														<?php $author = get_option('pinno_author_box'); if ($author == "true") { ?>
															<div id="pinno-author-box-wrap" class="left relative">
																<div class="pinno-author-box-out right relative">
																	<div id="pinno-author-box-img" class="left relative">
																		<?php echo get_avatar( get_the_author_meta('email'), '60' ); ?>
																	</div><!--pinno-author-box-img-->
																	<div class="pinno-author-box-in">
																		<div id="pinno-author-box-head" class="left relative">
																			<span class="pinno-author-box-name left relative"><?php the_author_posts_link(); ?></span>
																			<div id="pinno-author-box-soc-wrap" class="left relative">
																				<?php $pinno_email = get_option('pinno_author_email'); if ($pinno_email == "true") { ?>
																					<a href="mailto:<?php the_author_meta('email'); ?>"><span class="pinno-author-box-soc fa fa-envelope-square fa-2"></span></a>
																				<?php } ?>
																				<?php $authordesc = get_the_author_meta( 'facebook' ); if ( ! empty ( $authordesc ) ) { ?>
																					<a href="<?php the_author_meta('facebook'); ?>" alt="Facebook" target="_blank"><span class="pinno-author-box-soc fa fa-facebook-square fa-2"></span></a>
																				<?php } ?>
																				<?php $authordesc = get_the_author_meta( 'twitter' ); if ( ! empty ( $authordesc ) ) { ?>
																					<a href="<?php the_author_meta('twitter'); ?>" alt="Twitter" target="_blank"><span class="pinno-author-box-soc fa fa-twitter-square fa-2"></span></a>
																				<?php } ?>
																				<?php $authordesc = get_the_author_meta( 'pinterest' ); if ( ! empty ( $authordesc ) ) { ?>
																					<a href="<?php the_author_meta('pinterest'); ?>" alt="Pinterest" target="_blank"><span class="pinno-author-box-soc fa fa-pinterest-square fa-2"></span></a>
																				<?php } ?>
																				<?php $authordesc = get_the_author_meta( 'googleplus' ); if ( ! empty ( $authordesc ) ) { ?>
																					<a href="<?php the_author_meta('googleplus'); ?>" alt="Google Plus" target="_blank"><span class="pinno-author-box-soc fa fa-google-plus-square fa-2"></span></a>
																				<?php } ?>
																				<?php $authordesc = get_the_author_meta( 'instagram' ); if ( ! empty ( $authordesc ) ) { ?>
																					<a href="<?php the_author_meta('instagram'); ?>" alt="Instagram" target="_blank"><span class="pinno-author-box-soc fa fa-instagram fa-2"></span></a>
																				<?php } ?>
																				<?php $authordesc = get_the_author_meta( 'linkedin' ); if ( ! empty ( $authordesc ) ) { ?>
																					<a href="<?php the_author_meta('linkedin'); ?>" alt="LinkedIn" target="_blank"><span class="pinno-author-box-soc fa fa-linkedin-square fa-2"></span></a>
																				<?php } ?>
																			</div><!--pinno-author-box-soc-wrap-->
																		</div><!--pinno-author-box-head-->
																	</div><!--pinno-author-box-in-->
																</div><!--pinno-author-box-out-->
																<div id="pinno-author-box-text" class="left relative">
																	<p><?php the_author_meta('description'); ?></p>
																</div><!--pinno-author-box-text-->
															</div><!--pinno-author-box-wrap-->
														<?php } ?>
														<div class="pinno-org-wrap" itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
															<div class="pinno-org-logo" itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
																<?php if(get_option('pinno_logo')) { ?>
																	<img src="<?php echo esc_url(get_option('pinno_logo')); ?>"/>
																	<meta itemprop="url" content="<?php echo esc_url(get_option('pinno_logo')); ?>">
																<?php } else { ?>
																	<img src="<?php echo get_template_directory_uri(); ?>/images/logos/logo-nav.png" alt="<?php bloginfo( 'name' ); ?>" />
																	<meta itemprop="url" content="<?php echo get_template_directory_uri(); ?>/images/logos/logo-nav.png">
																<?php } ?>
															</div><!--pinno-org-logo-->
															<meta itemprop="name" content="<?php bloginfo( 'name' ); ?>">
														</div><!--pinno-org-wrap-->
													</div><!--pinno-content-bot-->
												</div><!--pinno-content-body-top-->
												<div class="pinno-cont-read-wrap">
													<?php $pinno_cont_read = get_option('pinno_cont_read'); if ($pinno_cont_read == "true") { ?>
														<div class="pinno-cont-read-but-wrap left relative">
															<span class="pinno-cont-read-but"><?php esc_html_e( 'Continue Reading', 'iggy-type-0' ); ?></span>
														</div><!--pinno-cont-read-but-wrap-->
													<?php } ?>
													
													<?php $pinno_related = get_option('pinno_related_posts'); if ($pinno_related == "true") { ?>
														<div id="pinno-related-posts" class="left relative">
															<h4 class="pinno-widget-home-title">
																<span class="pinno-widget-home-title"><?php esc_html_e( 'You may like', 'iggy-type-0' ); ?></span>
															</h4>
															<?php pinno_RelatedPosts(); ?>
														</div><!--pinno-related-posts-->
                                                    <?php } ?>
                                                    <?php // $pinno_post_ad = get_option('pinno_post_ad'); if ($pinno_post_ad) { ?>
														<div id="pinno-post-bot-ad" class="left relative">
															<span class="pinno-ad-label"><?php esc_html_e( 'Advertisement', 'iggy-type-0' ); ?></span>
															<?php // $pinno_post_ad = get_option('pinno_post_ad'); if ($pinno_post_ad) { echo html_entity_decode($pinno_post_ad); } ?>
															<div class="pinno-ad-serving" data-post-id="99<?php the_ID(); ?>">
 															<div id="div-gpt-ad-mm-default-top-b-99<?php the_ID(); ?>"></div>
															</div>
														</div><!--pinno-post-bot-ad-->
													<?php // } ?>
													<?php if ( comments_open() ) { ?>
														<?php $disqus_id = get_option('pinno_disqus_id'); if ($disqus_id) { if (isset($disqus_id)) {  ?>
															<div id="pinno-comments-button" class="left relative pinno-com-click">
																<span class="pinno-comment-but-text"><?php comments_number(__( 'Comments', 'iggy-type-0'), esc_html__('Comments', 'iggy-type-0'), esc_html__('Comments', 'iggy-type-0')); ?></span>
															</div><!--pinno-comments-button-->
															<?php $disqus_id2 = esc_html($disqus_id); pinno_disqus_embed($disqus_id2); ?>
														<?php } } else { ?>
															<div id="pinno-comments-button" class="left relative pinno-com-click">
																<span class="pinno-comment-but-text"><?php comments_number(__( 'Click to comment', 'iggy-type-0'), esc_html__('1 Comment', 'iggy-type-0'), esc_html__('% Comments', 'iggy-type-0'));?></span>
															</div><!--pinno-comments-button-->
															<?php comments_template(); ?>
														<?php } ?>
													<?php } ?>
												</div><!--pinno-cont-read-wrap-->
											</div><!--pinno-content-body-->
										</div><!--pinno-post-soc-in-->
									</div><!--pinno-post-soc-out-->
								</div><!--pinno-content-wrap-->
						<?php $pinno_more_posts = get_option('pinno_more_posts'); if ($pinno_more_posts == "true") { ?>
							<div id="pinno-post-add-box">
								<div id="pinno-post-add-wrap" class="left relative">
									<?php global $post; $pinno_more_num = esc_html(get_option('pinno_more_num')); $category = get_the_category(); $current_cat = $category[0]->cat_ID; $recent = new WP_Query(array( 'cat' => $current_cat, 'posts_per_page' => $pinno_more_num, 'ignore_sticky_posts'=> 1, 'post__not_in' => array( $post->ID ) )); while($recent->have_posts()) : $recent->the_post(); ?>
										<div class="pinno-post-add-story left relative">
											<div class="pinno-post-add-head left relative">
												<h3 class="pinno-post-cat left relative"><a class="pinno-post-cat-link" href="<?php $category = get_the_category(); $category_id = get_cat_ID( $category[0]->cat_name ); $category_link = get_category_link( $category_id ); echo esc_url( $category_link ); ?>"><span class="pinno-post-cat left"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span></a></h3>
												<h1 class="pinno-post-title left"><?php the_title(); ?></h1>
												<?php if ( has_excerpt() ) { ?>
													<span class="pinno-post-excerpt left"><?php the_excerpt(); ?></span>
												<?php } ?>
												<?php $author_info = get_option('pinno_author_info'); if ($author_info == "true") { ?>
													<div class="pinno-author-info-wrap left relative">
														<div class="pinno-author-info-thumb left relative">
															<?php echo get_avatar( get_the_author_meta('email'), '46' ); ?>
														</div><!--pinno-author-info-thumb-->
														<div class="pinno-author-info-text left relative">
															<div class="pinno-author-info-date left relative">
																<p><?php esc_html_e( 'Published', 'iggy-type-0' ); ?></p> <span class="pinno-post-date"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span> <p><?php esc_html_e( 'on', 'iggy-type-0' ); ?></p> <span class="pinno-post-date"><?php the_time(get_option('date_format')); ?></span>
															</div><!--pinno-author-info-date-->
															<div class="pinno-author-info-name left relative">
																<p><?php esc_html_e( 'By', 'iggy-type-0' ); ?></p> <span class="author-name vcard fn author" itemprop="name"><?php the_author_posts_link(); ?></span> <?php $authortwit = get_the_author_meta('twitter'); if ( ! empty ( $authortwit ) ) { ?><a href="<?php echo esc_url(the_author_meta('twitter')); ?>" class="pinno-twit-but" target="_blank"><span class="pinno-author-info-twit-but"><i class="fa fa-twitter fa-2"></i></span></a><?php } ?>
															</div><!--pinno-author-info-name-->
														</div><!--pinno-author-info-text-->
													</div><!--pinno-author-info-wrap-->
												<?php } ?>
											</div><!--pinno-post-add-head-->
											<div class="pinno-post-add-body left relative">
												<?php $pinno_featured_img = get_option('pinno_featured_img'); $pinno_show_hide = get_post_meta($post->ID, "pinno_featured_image", true); if ($pinno_featured_img == "true") { if ($pinno_show_hide !== "hide") { ?>
													<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
														<div class="pinno-post-add-img left relative">
															<?php the_post_thumbnail(''); ?>
														</div><!--pinno-post-feat-img-->
														<?php global $post; if(get_post_meta($post->ID, "pinno_photo_credit", true)): ?>
															<span class="pinno-feat-caption"><?php echo wp_kses_post(get_post_meta($post->ID, "pinno_photo_credit", true)); ?></span>
														<?php endif; ?>
													<?php } ?>
												<?php } } ?>
												<div class="pinno-post-add-cont left relative">
													<div class="pinno-post-add-main right relative" data-post-id="<?php the_id(); ?>">
														<?php the_content(); ?>
													</div><!--pinno-post-add-main-->
													<div class="pinno-post-add-link">
														<a href="<?php the_permalink(); ?>" rel="bookmark"><span class="pinno-post-add-link-but"><?php esc_html_e( "Continue Reading", 'iggy-type-0' ); ?></span></a>
													</div><!--pinno-post-add-link-->
												</div><!--pinno-post-add-cont-->
											</div><!--pinno-post-add-body-->
										</div><!--pinno-post-add-story-->
									<?php endwhile; wp_reset_postdata(); ?>
								</div><!--pinno-post-add-wrap-->
							</div><!--pinno-post-add-box-->
						<?php } ?>
							</div><!--pinno-post-content-->
						</div><!--pinno-post-main-in-->
						<?php global $post; $pinno_post_layout = get_option('pinno_post_layout'); $pinno_post_temp = get_post_meta($post->ID, "pinno_post_template", true); if( ( empty($pinno_post_temp) && empty($pinno_post_layout) ) || ( empty($pinno_post_temp) && $pinno_post_layout == '0' ) || ( $pinno_post_temp == "def" && $pinno_post_layout == '0' ) || ( empty($pinno_post_temp) && $pinno_post_layout == '2' ) || ( $pinno_post_temp == "def" && $pinno_post_layout == '2' ) || ( empty($pinno_post_temp) && $pinno_post_layout == '4' ) || ( $pinno_post_temp == "def" && $pinno_post_layout == '4' ) || ( empty($pinno_post_temp) && $pinno_post_layout == '6' ) || ( $pinno_post_temp == "def" && $pinno_post_layout == '6' ) || ( $pinno_post_temp == "global" && $pinno_post_layout == '0' ) || ( $pinno_post_temp == "global" && $pinno_post_layout == '2' ) || ( $pinno_post_temp == "global" && $pinno_post_layout == '4' ) || ( $pinno_post_temp == "global" && $pinno_post_layout == '6' ) || $pinno_post_temp == "temp1" || $pinno_post_temp == "temp3" || $pinno_post_temp == "temp5" || $pinno_post_temp == "temp7" ) { ?>
							<?php get_sidebar(); ?>
						<?php } ?>
					</div><!--pinno-post-main-out-->
				</div><!--pinno-post-main-->
			<?php $pinno_trend_posts = get_option('pinno_trend_posts'); if ($pinno_trend_posts == "true") { ?>
				<div id="pinno-post-more-wrap" class="left relative">
					<h4 class="pinno-widget-home-title">
						<span class="pinno-widget-home-title"><?php echo esc_html(get_option('pinno_pop_head')); ?></span>
					</h4>
					<ul class="pinno-post-more-list left relative">
						<?php global $post; $pinno_trend_post_num = get_option('pinno_trend_post_num'); $pop_days = esc_html(get_option('pinno_pop_days')); $popular_days_ago = "$pop_days days ago"; $recent = new WP_Query(array('posts_per_page' => $pinno_trend_post_num, 'ignore_sticky_posts'=> 1, 'post__not_in' => array( $post->ID ), 'orderby' => 'meta_value_num', 'order' => 'DESC', 'meta_key' => 'post_views_count', 'date_query' => array( array( 'after' => $popular_days_ago )) )); while($recent->have_posts()) : $recent->the_post(); ?>
							<a href="<?php the_permalink(); ?>" rel="bookmark">
							<li>
								<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
									<div class="pinno-post-more-img left relative">
										<?php the_post_thumbnail('pinno-mid-thumb', array( 'class' => 'pinno-reg-img' )); ?>
										<?php the_post_thumbnail('pinno-small-thumb', array( 'class' => 'pinno-mob-img' )); ?>
										<?php if ( has_post_format( 'video' )) { ?>
											<div class="pinno-vid-box-wrap pinno-vid-box-mid pinno-vid-marg">
												<i class="fa fa-2 fa-play" aria-hidden="true"></i>
											</div><!--pinno-vid-box-wrap-->
										<?php } else if ( has_post_format( 'gallery' )) { ?>
											<div class="pinno-vid-box-wrap pinno-vid-box-mid">
												<i class="fa fa-2 fa-camera" aria-hidden="true"></i>
											</div><!--pinno-vid-box-wrap-->
										<?php } ?>
									</div><!--pinno-post-more-img-->
								<?php } ?>
								<div class="pinno-post-more-text left relative">
									<div class="pinno-cat-date-wrap left relative">
										<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
									</div><!--pinno-cat-date-wrap-->
									<p><?php the_title(); ?></p>
								</div><!--pinno-post-more-text-->
							</li>
							</a>
						<?php endwhile; wp_reset_postdata(); ?>
					</ul>
				</div><!--pinno-post-more-wrap-->
			<?php } ?>
			</div><!--pinno-main-box-->
		</div><!--pinno-article-cont-->
	<?php setCrunchifyPostViews(get_the_ID()); ?>
	<?php endwhile; endif; ?>
</article><!--pinno-article-wrap-->
<?php get_footer(); ?>